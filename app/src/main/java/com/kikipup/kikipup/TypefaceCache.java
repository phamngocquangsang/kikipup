package com.kikipup.kikipup;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Created by phatnguyen on 8/28/16.
 */
public class TypefaceCache {
    public static String FONT_LIGHT = "fonts/OpenSans-Light.ttf";
    public static String FONT_REGULAR = "fonts/OpenSans-Regular.ttf";
    public static String FONT_BOLD = "fonts/RobotoSlab-Bold.ttf";
    public static String FONT_SEMI_BOLD = "fonts/OpenSans-Semibold.ttf";

    private static final Hashtable<String, Typeface> CACHE = new Hashtable<String, Typeface>();

    public static Typeface get(Context context, String name) {

        synchronized (CACHE) {

            if (!CACHE.containsKey(name)) {
                Typeface t = Typeface.createFromAsset(context.getAssets(), name);
                CACHE.put(name, t);
            }
            return CACHE.get(name);
        }
    }

}