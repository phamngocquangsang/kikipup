package com.kikipup.kikipup.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Question {

    public static List<Question> sDummyQuestion;
    static {
        sDummyQuestion = new ArrayList<>();
        List<QuestionOption> options = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            QuestionOption option = new QuestionOption("id"+i, "Option "+i, 0.25f);
            options.add(option);
        }

        for (int i = 0; i < 10; i++) {
            Question question = new Question("id"+i, "Where does it come from?", i % 2 == 0, i%2 == 0, options);
            sDummyQuestion.add(question);
        }
    }

    private String id;

    private String title;

    @SerializedName("is_multiple_response")
    private boolean isMultipleResponse;

    @SerializedName("is_mandatory")
    private boolean isMandatory;

    @SerializedName("question_options")
    private List<QuestionOption> questionOptions;

    //this flag use to check if we should show poll result or not
    //only when after user submit their answer to server successfully.
    private boolean isShownResults;

    //represent user answer question <optionsId, isSelected>,
    //use Map over List for faster checking.
    private List<String> answers;

    public Question(String id, String title, boolean isMultipleResponse, boolean isMandatory, List<QuestionOption> questionOptions) {
        this.id = id;
        this.title = title;
        this.isMultipleResponse = isMultipleResponse;
        this.isMandatory = isMandatory;
        this.questionOptions = questionOptions;
        this.isShownResults = false;
        this.answers = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isMultipleResponse() {
        return isMultipleResponse;
    }

    public void setMultipleResponse(boolean multipleResponse) {
        isMultipleResponse = multipleResponse;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public List<QuestionOption> getQuestionOptions() {
        return questionOptions;
    }

    public void setQuestionOptions(List<QuestionOption> questionOptions) {
        this.questionOptions = questionOptions;
    }

    public boolean isShownResults() {
        return isShownResults;
    }

    public void setShownResults(boolean shownResults) {
        isShownResults = shownResults;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
