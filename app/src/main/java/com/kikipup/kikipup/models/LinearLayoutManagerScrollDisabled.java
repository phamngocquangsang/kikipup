package com.kikipup.kikipup.models;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

public class LinearLayoutManagerScrollDisabled extends LinearLayoutManager {

    private boolean isScrollable;

    public LinearLayoutManagerScrollDisabled(Context context) {
        super(context);
    }

    public LinearLayoutManagerScrollDisabled(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public LinearLayoutManagerScrollDisabled(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean canScrollHorizontally() {
        return super.canScrollHorizontally() && isScrollable;
    }

    public boolean isScrollable() {
        return isScrollable;
    }

    public void setScrollable(boolean scrollable) {
        isScrollable = scrollable;
    }
}
