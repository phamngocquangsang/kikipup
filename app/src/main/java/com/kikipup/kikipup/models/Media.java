package com.kikipup.kikipup.models;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Media implements Parcelable {

    public static final int TYPE_IMAGE = 0;

    public static final int TYPE_YOUTUBE = 1;

    public static final int TYPE_LINK_SHARE = 2;

    private int type;

    private String mediaUrl;

    private int width;

    private int height;

    public Media(int type, String mediaUrl, int width, int height) {
        this.type = type;
        this.mediaUrl = mediaUrl;
        this.width = width;
        this.height = height;
    }

    public Media() {
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeString(this.mediaUrl);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    protected Media(Parcel in) {
        this.type = in.readInt();
        this.mediaUrl = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Parcelable.Creator<Media> CREATOR = new Parcelable.Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel source) {
            return new Media(source);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    public static Media getFakeMedia() {
        Random random = new Random();
        int randomint = random.nextInt(8);
        switch (randomint){
            case 0:
                return new Media(Media.TYPE_IMAGE, "https://s-media-cache-ak0.pinimg.com/736x/92/9d/3d/929d3d9f76f406b5ac6020323d2d32dc.jpg", 463, 695);
            case 1:
                return new Media(TYPE_IMAGE, "https://media.giphy.com/media/r6uEVfPTT7PYk/giphy.gif", 480, 269);
            case 2:
                return new Media(TYPE_IMAGE, "https://static.pexels.com/photos/7720/night-animal-dog-pet.jpg", 675, 450);
            case 3:
                return new Media(TYPE_IMAGE, "http://www.cutecatgifs.com/wp-content/uploads/2015/11/Funny-Cat-vacuum.gif", 300, 382);
            case 4:
                return new Media(TYPE_IMAGE, "http://ell.h-cdn.co/assets/16/41/1476456910-polls.gif", 500, 281);
            case 5:
                return new Media(TYPE_IMAGE, "http://www.rd.com/wp-content/uploads/sites/2/2016/02/01-13-things-you-didnt-know-about-cats-earthquakes.jpg", 675, 448);
            default:
                return new Media(TYPE_IMAGE, "https://www.bluecross.org.uk/sites/default/files/assets/images/20311lpr.jpg",675, 449);
        }
    }

    public static Map<String, Object> toMap(@Nullable Media media){
        if(media == null){
            return null;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("type", media.type);
        map.put("mediaUrl", media.mediaUrl);
        map.put("width", media.width);
        map.put("height", media.height);
        return map;
    }


}
