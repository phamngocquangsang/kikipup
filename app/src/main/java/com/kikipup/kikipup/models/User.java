package com.kikipup.kikipup.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.kikipup.kikipup.Constants;
import com.kikipup.kikipup.Utilities;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Quang Quang on 7/17/2016.
 */
public class User implements Parcelable{
    private static final String TAG = User.class.getSimpleName();
    public static String TIME_JOINED_KEY = "timestamp";


    private String userId;
    private String email;
    private String name;
    private HashMap<String, Object> timeJoined;
    private int likeCount;
    private int dislikeCount;
    private int unhelpfulCount;
    private int helpfulCount;
    private String profileImageUrl;
    private boolean isAdmin;
    private HashMap<String, Object> categories;
    private String OSUserId;
    private String registrationId;

    public User(String userId, String email, String name, HashMap<String, Object> timeJoined, int likeCount, int dislikeCount, int unhelpfulCount, int helpfulCount, String profileImageUrl, boolean isAdmin, HashMap<String, Object> categories, String OSUserId, String registrationId) {
        this.userId = userId;
        this.email = email;
        this.name = name;
        this.timeJoined = timeJoined;
        this.likeCount = likeCount;
        this.dislikeCount = dislikeCount;
        this.unhelpfulCount = unhelpfulCount;
        this.helpfulCount = helpfulCount;
        this.profileImageUrl = profileImageUrl;
        this.isAdmin = isAdmin;
        this.categories = categories;
        this.OSUserId = OSUserId;
        this.registrationId = registrationId;
    }

    public User(){
        likeCount=0;
        helpfulCount = 0;
        unhelpfulCount = 0;
        dislikeCount = 0;
        isAdmin = false;
    }

    public boolean isEquals(User user) {
        return this.userId.equals(user.getUserId());
    }

    public static User getTemporaryUser(){

        Random random = new Random();
        String randomAvatar = "avatar_"+ (random.nextInt(16)+1);

        User temporaryUser =
                new User(String.valueOf(System.currentTimeMillis()), Constants.ANONYMOUS_EMAIL,"Anonymous",null,0,0,0,0,randomAvatar,false,null,"","");

        return temporaryUser;
    }


    public boolean isTemporaryUser(){
        return email.equals(Constants.ANONYMOUS_EMAIL);
    }

    public boolean isFakeUser(){
        String[] strings = Utilities.decodeEmail(getEmail()).split("@");
        return strings[1].equals(Constants.ANONYMOUS_EMAIL_DOMAIN);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, Object> getTimeJoined() {
        return timeJoined;
    }

    public void setTimeJoined(HashMap<String, Object> timeJoined) {
        this.timeJoined = timeJoined;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getHelpfulCount() {
        return helpfulCount;
    }

    public void setHelpfulCount(int helpfulCount) {
        this.helpfulCount = helpfulCount;
    }

    public HashMap<String, Object> getCategories() {
        return categories;
    }

    public void setCategories(HashMap<String, Object> categories) {
        this.categories = categories;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(int dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public int getUnhelpfulCount() {
        return unhelpfulCount;
    }

    public void setUnhelpfulCount(int unhelpfulCount) {
        this.unhelpfulCount = unhelpfulCount;
    }

    public String getOSUserId() {
        return OSUserId;
    }

    public void setOSUserId(String OSUserId) {
        this.OSUserId = OSUserId;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean admin) {
        isAdmin = admin;
    }

    public HashMap<String, Object> toMap(){
        HashMap<String, Object> userMap = new HashMap<String, Object>();
        userMap.put(Constants.FIREBASE_PROPERTY_USER_ID, userId);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_EMAIL,Utilities.encodeEmail(email));
        userMap.put(Constants.FIREBASE_PROPERTY_USER_NAME, name);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_TIME_JOINED,timeJoined);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_PROFILE_URL, profileImageUrl);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_LIKE_COUNT,likeCount);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_DISLIKE_COUNT,dislikeCount);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_HELPFUL_COUT,helpfulCount);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_UNHELPFUL_COUT,unhelpfulCount);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_INTEREST_CATEGORIES,categories);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_IS_ADMIN, isAdmin);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_OS_ID,OSUserId);
        userMap.put(Constants.FIREBASE_PROPERTY_USER_REGISTRATION_ID,registrationId);

        return userMap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.email);
        dest.writeString(this.name);
        dest.writeSerializable(this.timeJoined);
        dest.writeInt(this.likeCount);
        dest.writeInt(this.dislikeCount);
        dest.writeInt(this.unhelpfulCount);
        dest.writeInt(this.helpfulCount);
        dest.writeString(this.profileImageUrl);
        dest.writeByte(this.isAdmin ? (byte) 1 : (byte) 0);
        dest.writeSerializable(this.categories);
        dest.writeString(this.OSUserId);
        dest.writeString(this.registrationId);
    }

    protected User(Parcel in) {
        this.userId = in.readString();
        this.email = in.readString();
        this.name = in.readString();
        this.timeJoined = (HashMap<String, Object>) in.readSerializable();
        this.likeCount = in.readInt();
        this.dislikeCount = in.readInt();
        this.unhelpfulCount = in.readInt();
        this.helpfulCount = in.readInt();
        this.profileImageUrl = in.readString();
        this.isAdmin = in.readByte() != 0;
        this.categories = (HashMap<String, Object>) in.readSerializable();
        this.OSUserId = in.readString();
        this.registrationId = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}