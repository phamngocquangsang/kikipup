package com.kikipup.kikipup.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Reaction implements Parcelable {

    private int likeCount;

    private int dislikeCount;

    private int loveCount;

    private int laughCount;

    private int sadCount;

    public Reaction(int likeCount, int dislikeCount, int loveCount, int laughCount, int sadCount) {
        this.likeCount = likeCount;
        this.dislikeCount = dislikeCount;
        this.loveCount = loveCount;
        this.laughCount = laughCount;
        this.sadCount = sadCount;
    }

    public Reaction() {

    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(int dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public int getLoveCount() {
        return loveCount;
    }

    public void setLoveCount(int loveCount) {
        this.loveCount = loveCount;
    }

    public int getLaughCount() {
        return laughCount;
    }

    public void setLaughCount(int laughCount) {
        this.laughCount = laughCount;
    }

    public int getSadCount() {
        return sadCount;
    }

    public void setSadCount(int sadCount) {
        this.sadCount = sadCount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.likeCount);
        dest.writeInt(this.dislikeCount);
        dest.writeInt(this.loveCount);
        dest.writeInt(this.laughCount);
        dest.writeInt(this.sadCount);
    }

    protected Reaction(Parcel in) {
        this.likeCount = in.readInt();
        this.dislikeCount = in.readInt();
        this.loveCount = in.readInt();
        this.laughCount = in.readInt();
        this.sadCount = in.readInt();
    }

    public static final Parcelable.Creator<Reaction> CREATOR = new Parcelable.Creator<Reaction>() {
        @Override
        public Reaction createFromParcel(Parcel source) {
            return new Reaction(source);
        }

        @Override
        public Reaction[] newArray(int size) {
            return new Reaction[size];
        }
    };
}
