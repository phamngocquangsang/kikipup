package com.kikipup.kikipup.models;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kikipup.kikipup.Constants;
import com.kikipup.kikipup.Utilities;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Post implements Comparable<Post> {

    public static final String CATEGORY_DOG = "dog";
    public static final String CATEGORY_CAT = "cat";
    public static final String CATEGORY_OTHER = "other";

    private static final String TAG = Post.class.getSimpleName();

    private String postPid;

    private User user;

    private Map<String,Object> lastTimeEdited;

    private String title;

    private String content;

    private Media media;

    private Reaction reactions;

    private int commentCount;

    private String category;

    private List<String> mentions;

    private int readCount;

    private Map<String, Object> timeCreated;

    public Post() {
        reactions = new Reaction();
    }

    public static Map<String, Object> toMap(Post post){
        Map<String, Object> value = new HashMap<>();

        value.put(Constants.FIREBASE_PROPERTY_POST_WRITTEN_BY,post.getUser().toMap());
        value.put(Constants.FIREBASE_PROPERTY_POST_POST_ID,post.getPostPid());
        value.put(Constants.FIREBASE_PROPERTY_POST_REACTION,post.getReactions());
        value.put(Constants.FIREBASE_PROPERTY_POST_COMMENT_COUNT,post.getCommentCount());
        value.put(Constants.FIREBASE_PROPERTY_POST_TITLE,post.getTitle());
        value.put(Constants.FIREBASE_PROPERTY_POST_CONTENT, post.getContent());
        value.put(Constants.FIREBASE_PROPERTY_POST_LAST_TIME_EDITED,post.getLastTimeEdited());
        value.put(Constants.FIREBASE_PROPERTY_POST_TIME_CREATED,post.getTimeCreated());
        value.put(Constants.FIREBASE_PROPERTY_POST_MENTION_PEOPLE,post.getMentions());
        value.put(Constants.FIREBASE_PROPERTY_POST_CATEGORY,post.getCategory());
        value.put(Constants.FIREBASE_PROPERTY_POST_MEDIA, Media.toMap(post.getMedia()));
        value.put(Constants.FIREBASE_PROPERTY_POST_READ_COUNT, post.getReadCount());
        return value;
    }

    public Map<String, Object> getTimeCreated() {
        return timeCreated;
    }


    public static Post getFakePost(){
        Random random = new Random();
        Post post = new Post();
        post.setCategory(Post.CATEGORY_CAT);
        post.setCommentCount(random.nextInt(450));
        post.setContent("This is fake content");
        post.setTitle("This is Fake title");
        Map<String, Object> timeEdited = new HashMap<>();
        timeEdited.put(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP, System.currentTimeMillis());
        post.setLastTimeEdited(timeEdited);
        post.setMedia(Media.getFakeMedia());
        post.setPostPid(String.valueOf(System.currentTimeMillis()));
        post.setReactions(new Reaction(random.nextInt(250), random.nextInt(500), random.nextInt(400), random.nextInt(1000), random.nextInt(100)));
        post.setReadCount(random.nextInt(10000));
        Map<String, Object> timeCreated = new HashMap<>();
        timeCreated.put(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP, System.currentTimeMillis());
        post.setTimeCreated(timeCreated);
        post.setUser(User.getTemporaryUser());
        return post;
    }


    public static void updatePost(final Post newPost){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Map<String,Object> value = Post.toMap(newPost);
        HashMap<String, Object> childUpdate = new HashMap<>();
        childUpdate.put("/"+Constants.LOCATION_POSTS +"/"+newPost.getPostPid()+"/",value);
        childUpdate.put("/"+Constants.LOCATION_USER_POSTS +"/"+Utilities.encodeEmail(newPost.getUser().getEmail())+"/"+newPost.getPostPid()+"/",value);
        ref.updateChildren(childUpdate, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Log.i(TAG, "onComplete: update Post: "+ newPost.getPostPid()+" completed");
            }
        });
    }

    public static void postNewConfession(final Context c, final Post confession, @Nullable final DatabaseReference.CompletionListener listener){
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        String key = database.child(Constants.LOCATION_POSTS).push().getKey();
        confession.setPostPid(key);
        Map<String, Object> postValue = Post.toMap(confession);
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/"+Constants.LOCATION_POSTS +"/"+key,postValue);
        childUpdates.put("/"+Constants.LOCATION_USER_POSTS +"/"+ Utilities.encodeEmail(confession.getUser().getEmail())+"/"+key,postValue);
        Log.i(TAG, "postNewConfession: "+childUpdates.toString());
        database.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
            }
        });
    }

    public static void deleteConfession(final Post confession, @Nullable final DatabaseReference.CompletionListener listener){
        final DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        String key = confession.getPostPid();
        HashMap<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/"+Constants.LOCATION_POSTS +"/"+key,null);
        childUpdates.put("/"+Constants.LOCATION_USER_POSTS +"/"+ Utilities.encodeEmail(confession.getUser().getEmail())+"/"+key,null);
        Log.i(TAG, "deleteConfession: "+childUpdates.toString());
        database.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
            }
        });
        //TODO maybe we should also delete all the comment of this post
        //TODO if this confession has a lot of comments, delete would take time
    }

    public String getPostPid() {
        return postPid;
    }

    public void setPostPid(String postPid) {
        this.postPid = postPid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public long getTimeCreatedAsLong(){
        return ((long)
                timeCreated.get(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP));
    }

    public void setTimeCreated(Map<String, Object> timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Map<String, Object> getLastTimeEdited() {
        return lastTimeEdited;
    }

    public void setLastTimeEdited(Map<String, Object> lastTimeEdited) {
        this.lastTimeEdited = lastTimeEdited;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Reaction getReactions() {
        return reactions;
    }

    public void setReactions(Reaction reactions) {
        this.reactions = reactions;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public void setMentions(List<String> mentions) {
        this.mentions = mentions;
    }

    public int getReadCount() {
        return readCount;
    }

    public void setReadCount(int readCount) {
        this.readCount = readCount;
    }

    @Override
    public int compareTo(Post post) {
        long timeCreate = Long.valueOf(timeCreated.get(Constants.FIREBASE_PROPERTY_POST_TIME_CREATED).toString());
        long timeCreate2 = Long.valueOf(post.getTimeCreated().get(Constants.FIREBASE_PROPERTY_POST_TIME_CREATED).toString());
        if(timeCreate==timeCreate2){
            return 0;
        }
        return (timeCreate>timeCreate2?1:2);
    }

    public static Comparator<Post> getTimeCreatedCompator(){
        return new Comparator<Post>() {
            @Override
            public int compare(Post post, Post t1) {
                long timeCreate, timeCreate2;
                try{
                    timeCreate = Long.valueOf(post.getTimeCreated().get(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP).toString());
                    timeCreate2 = Long.valueOf(t1.getTimeCreated().get(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP).toString());
                }catch (NumberFormatException exception){
                    Log.e(TAG, "compare: invalid number format exception", exception);
//                    FirebaseCrash.report(exception);
                    return 0;
                }

                if (timeCreate == timeCreate2) {
                    return 0;
                }
                return (timeCreate > timeCreate2 ? -1 : 1);
            }
        };
    }



}
