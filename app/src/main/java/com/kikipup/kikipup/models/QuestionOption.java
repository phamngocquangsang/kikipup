package com.kikipup.kikipup.models;

public class QuestionOption {

    private String id;

    private String title;

    private float voteResult;

    public QuestionOption(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public QuestionOption(String id, String title, float voteResult) {
        this.id = id;
        this.title = title;
        this.voteResult = voteResult;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getVoteResult() {
        return voteResult;
    }

    public void setVoteResult(float voteResult) {
        this.voteResult = voteResult;
    }
}
