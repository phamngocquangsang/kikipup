package com.kikipup.kikipup.login;

import android.content.Intent;

import com.google.firebase.auth.FirebaseUser;
import com.kikipup.kikipup.BasePresenter;
import com.kikipup.kikipup.BaseView;
import com.kikipup.kikipup.models.User;

interface LoginContract {

    interface View extends BaseView<Presenter> {

        void signOut();

        void showErrorConnectToGoogleFailed();

        void setLoadingIndicator(boolean active);

        void openEditProfileScreen(User userObject);

        void openMainApp(String userObject, boolean enterNewsFeed);
    }

    interface Presenter extends BasePresenter{

        void result(int requestCode, int resultCode, Intent data);

        void writeNewUser(User userObject);

        void saveLoggedUser(User userObject);

        void setUserLogged(boolean b);

        void onFirebaseAuthStateChanged(FirebaseUser user);
    }
}
