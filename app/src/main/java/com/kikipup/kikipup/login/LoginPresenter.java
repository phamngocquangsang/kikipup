package com.kikipup.kikipup.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.kikipup.kikipup.KikiPupApp;
import com.kikipup.kikipup.Utilities;
import com.kikipup.kikipup.data.DataRepository;
import com.kikipup.kikipup.data.DataSource;
import com.kikipup.kikipup.data.LocalSetting;
import com.kikipup.kikipup.models.User;

import java.util.HashMap;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;

public class LoginPresenter implements LoginContract.Presenter{

    private static final String TAG = LoginPresenter.class.getSimpleName();


    private final LoginContract.View mLoginView;

    @Inject
    DataSource mDataSource;

    @Inject
    LocalSetting mLocalSetting;

    public LoginPresenter(@NonNull LoginContract.View loginView) {
        checkNotNull(loginView, "loginView cannot be null");
        mLoginView = loginView;
        mLoginView.setPresenter(this);
        KikiPupApp.getAppComponentInstance().inject(this);
    }

    @Override
    public void result(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void start() {
    }




    @Override
    public void stop() {
    }

    @Override
    public void writeNewUser(final User userObject) {
        mDataSource.createNewUser(userObject, new DataSource.OnResponse<User>() {
            @Override
            public void onSuccess(User type) {
                Log.i(TAG, "onSuccess: write new user id: "+userObject.getUserId()+ " succeed");
            }

            @Override
            public void onFailure(Exception exception) {

            }
        });
    }

    @Override
    public void saveLoggedUser(User userObject) {
        mLocalSetting.saveLoggedUser(userObject);
    }

    @Override
    public void setUserLogged(boolean b) {
        mLocalSetting.setUserLogged(b);
    }

    @Override
    public void onFirebaseAuthStateChanged(final FirebaseUser user) {
        if (user != null) {
            // User is signed in
            Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

            mDataSource.getUser(user.getUid(), new DataSource.OnResponse<User>() {
                @Override
                public void onSuccess(User userObject) {
                    if (userObject == null) {//user's not exist
                        userObject = User.getTemporaryUser();
                        userObject.setEmail(user.getEmail());
                        userObject.setUserId(user.getUid());
                        userObject.setName(user.getDisplayName());
                        if (user.getPhotoUrl() != null) {
                            userObject.setProfileImageUrl(user.getPhotoUrl().toString());
                        }

                        HashMap<String, Object> timeJoined = new HashMap<>();
                        timeJoined.put(User.TIME_JOINED_KEY, ServerValue.TIMESTAMP);
                        userObject.setTimeJoined(timeJoined);
                        LoginPresenter.this.writeNewUser(userObject);
                        Log.d(TAG, "onDataChange: After write user");
                    }
                    saveLoggedUser(userObject);
                    setUserLogged(true);
                    if(userObject.getName()==null){
                        mLoginView.openEditProfileScreen(userObject);
                    }else{
                        mLoginView.openMainApp(userObject.getUserId(), true);
                    }
                }

                @Override
                public void onFailure(Exception exception) {
                    Log.e(TAG, "onFailure: ", exception);
                }
            });

        } else {
            // User is signed out
            Log.d(TAG, "onAuthStateChanged:signed_out");
            LoginManager.getInstance().logOut();
            try{
                mLoginView.signOut();
            }catch (Exception e){
                Log.e(TAG, "onAuthStateChanged: "+e.getMessage() );
            }
        }
    }
}
