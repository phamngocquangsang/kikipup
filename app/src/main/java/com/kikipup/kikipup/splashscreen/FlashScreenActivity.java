package com.kikipup.kikipup.splashscreen;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.animation.Animation;
import com.kikipup.kikipup.login.LoginActivity;
import com.kikipup.kikipup.mainscreen.KikiActivity;


public class FlashScreenActivity extends AppCompatActivity {
    private static final String TAG = FlashScreenActivity.class.getSimpleName();
    private ImageView mFlashScreenImageView;
    private TextView mTitleTextView;

    private static final int DURATION = 300;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_screen);
        mFlashScreenImageView = (ImageView) findViewById(R.id.imageView_flash_screen);
        mTitleTextView = (TextView) findViewById(R.id.textView_flash_screen_title);

        AnimatorSet zoomOutImage = Animation.getZoomAnimation(mFlashScreenImageView,DURATION,new AccelerateDecelerateInterpolator(),1f,3f);
        AnimatorSet zoomOutTextView = Animation.getZoomAnimation(mTitleTextView,DURATION,new AccelerateDecelerateInterpolator(),1f,3f);

        AnimatorSet finalZoom = new AnimatorSet();
        finalZoom.setDuration(DURATION);
        finalZoom.setInterpolator(new AccelerateDecelerateInterpolator());
        finalZoom.playTogether(zoomOutImage,zoomOutTextView);
        finalZoom.start();

        finalZoom.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if(currentUser == null){// user hasn't sign in
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                }else{
                    Intent intent = KikiActivity.newIntent(FlashScreenActivity.this, currentUser.getUid(), true);
                    FlashScreenActivity.this.startActivity(intent);
                    // TODO: 5/23/17 start KikiPupuActivity
                }
            }
        });


    }
}
