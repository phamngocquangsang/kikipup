package com.kikipup.kikipup;

import android.app.Application;
import android.content.Intent;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.kikipup.kikipup.DI.AppComponent;
import com.kikipup.kikipup.DI.AppModule;
import com.kikipup.kikipup.DI.DaggerAppComponent;
import com.kikipup.kikipup.DI.DataModule;
import com.kikipup.kikipup.mainscreen.KikiActivity;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

public class KikiPupApp extends Application {

    static private AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        //facebook initialize
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        //Firebase initialize
        FirebaseApp.initializeApp(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        Fresco.initialize(this);
        OneSignal.NotificationOpenedHandler notificationOpenedHandler = new OneSignal.NotificationOpenedHandler() {
            @Override
            public void notificationOpened(OSNotificationOpenResult result) {
                Intent i = new Intent(getApplicationContext(), KikiActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };
        OneSignal.startInit(this).setNotificationOpenedHandler(notificationOpenedHandler).init();

        sAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).dataModule(new DataModule()).build();

    }

    public static AppComponent getAppComponentInstance(){
        return sAppComponent;
    }
}
