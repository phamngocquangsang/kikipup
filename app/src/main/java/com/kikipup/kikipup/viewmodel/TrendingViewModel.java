package com.kikipup.kikipup.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.kikipup.kikipup.KikiPupApp;
import com.kikipup.kikipup.data.DataSource;
import com.kikipup.kikipup.models.Post;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;


public class TrendingViewModel extends ViewModel {
    private String mUserId;

    @Inject
    DataSource mDataRepository;

    public TrendingViewModel() {
        KikiPupApp.getAppComponentInstance().inject(this);
    }

    public void initializeUserId(@NonNull String userId){
        if(userId == null){
            throw new IllegalStateException("userId can not be null");
        }
        mUserId = userId;
    }

    public LiveData<List<Post>> getFakeListPost(){
        return mDataRepository.loadUserNewsfeed(mUserId, 10);
    }

}
