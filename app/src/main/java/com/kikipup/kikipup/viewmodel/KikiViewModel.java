package com.kikipup.kikipup.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.kikipup.kikipup.KikiPupApp;
import com.kikipup.kikipup.data.DataRepository;
import com.kikipup.kikipup.data.DataSource;
import com.kikipup.kikipup.models.Post;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class KikiViewModel extends ViewModel{

    private String mUserId;

    @Inject
    DataSource mDataRepository;

    public KikiViewModel() {
        KikiPupApp.getAppComponentInstance().inject(this);
    }

    public void initializeUserId(String userId){
        mUserId = userId;
    }

    public LiveData<List<Post>> loadUserNewsFeed(){
        return mDataRepository.loadUserNewsfeed(mUserId, 10);
    }

    public LiveData<Map<String, Boolean>> getUserLikePosts() {
        return mDataRepository.loadUserLikePost(mUserId);
    }
}
