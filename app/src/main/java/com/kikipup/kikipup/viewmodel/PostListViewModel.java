package com.kikipup.kikipup.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.kikipup.kikipup.KikiPupApp;
import com.kikipup.kikipup.data.DataSource;
import com.kikipup.kikipup.models.Post;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class PostListViewModel extends ViewModel {

    private String mUserId;

    @Inject
    DataSource mDataRepository;

    public PostListViewModel() {
        KikiPupApp.getAppComponentInstance().inject(this);
    }

    public void initializeUserId(@NonNull String userId){
        if(userId == null){
            throw new IllegalStateException("userId can not be null");
        }
        mUserId = userId;
    }

    public LiveData<List<Post>> getFakeListPost(){
        return mDataRepository.loadUserNewsfeed(mUserId, 10);
    }

    public LiveData<Map<String, Boolean>> getUserLikePosts() {
        return mDataRepository.loadUserLikePost(mUserId);
    }

    public LiveData<Map<String, Boolean>> getUserDislikePosts() {
        return mDataRepository.loadUserDislikePost(mUserId);
    }

    public LiveData<Map<String, Boolean>> getUserLovePosts() {
        return mDataRepository.loadUserLovePost(mUserId);
    }

    public LiveData<Map<String, Boolean>> getUserSadPosts() {
        return mDataRepository.loadUserSadPost(mUserId);
    }

    public LiveData<Map<String, Boolean>> getUserLaughPosts() {
        return mDataRepository.loadUserLaughPost(mUserId);
    }

    public void likePost(String userId, String postPid) {
        mDataRepository.likePost(userId, postPid);
    }

    public void dislikePost(String userId, String postPid){
        mDataRepository.dislikePost(userId, postPid);
    }

    public void lovePost(String userId, String postPid){
        mDataRepository.lovePost(userId, postPid);
    }

    public void sadPost(String userId, String postPid){
        mDataRepository.sadPost(userId, postPid);
    }

    public void laughPost(String userId, String postPid){
        mDataRepository.laughPost(userId, postPid);
    }

    public void deletePost(String postPid) {
        mDataRepository.deletePost(postPid);
    }
}
