package com.kikipup.kikipup.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.kikipup.kikipup.KikiPupApp;
import com.kikipup.kikipup.data.DataSource;
import com.kikipup.kikipup.models.Post;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;


public class PostDetailViewModel extends ViewModel {

    @Inject
    DataSource mDataRepository;

    public PostDetailViewModel() {
        KikiPupApp.getAppComponentInstance().inject(this);
    }


    public LiveData<Post> loadPost(@NonNull String postId){
        checkNotNull(postId);
        return mDataRepository.loadPost(postId);
    }
}
