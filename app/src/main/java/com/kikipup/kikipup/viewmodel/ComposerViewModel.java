package com.kikipup.kikipup.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kikipup.kikipup.KikiPupApp;
import com.kikipup.kikipup.data.DataSource;
import com.kikipup.kikipup.livecycleaware.FirebaseLiveData;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;


public class ComposerViewModel extends ViewModel {

    private static final String TAG = ComposerViewModel.class.getSimpleName();

    private String mUserId;

    private String mPostId;

    private MediatorLiveData<Post> mLivePost;
    private MutableLiveData<String> pickedImage = new MutableLiveData<>();

    @Inject
    DataSource mDataRepository;

    public ComposerViewModel() {
        KikiPupApp.getAppComponentInstance().inject(this);
    }

    public void initializeUserId(String userId){
        mUserId = userId;
    }

    public void initializePostId(@Nullable String postId){
        mPostId = postId;
        mLivePost = new MediatorLiveData<>();
        if(postId == null){
            Post post = new Post();
            mLivePost.setValue(post);
            mDataRepository.loadDraftPost(new DataSource.OnResponse<Post>() {
                @Override
                public void onSuccess(Post type) {
                    mLivePost.setValue(type);
                }

                @Override
                public void onFailure(Exception exception) {

                }
            });
        }else{
            final LiveData<Post> previousPost = mDataRepository.loadPost(postId);
            mLivePost.addSource(previousPost, new Observer<Post>() {
                @Override
                public void onChanged(@Nullable Post post) {
                    if(post!=null){
                        mLivePost.setValue(post);
                        mLivePost.removeSource(previousPost);
                    }
                }
            });
        }
    }

    public MediatorLiveData<Post> getLivePost(){
        return mLivePost;
    }

    public void createPost(final Post post, final String userId) {
        Log.i(TAG, "createPost: ");
        mDataRepository.getUser(userId, new DataSource.OnResponse<User>() {
            @Override
            public void onSuccess(User type) {
                checkNotNull(type, "does not find user with id: "+userId);
                post.setUser(type);
                mDataRepository.createPost(post);
            }

            @Override
            public void onFailure(Exception exception) {

            }
        });

    }

    LiveData<Post> loadUserPost(String postId){
        return mDataRepository.loadPost(postId);
    }

    public void updatePost(Post updatedPost) {
        mDataRepository.updatePost(updatedPost);
    }

    public void setPickedImage(String image){
        pickedImage.setValue(image);
    }

    public MutableLiveData<String> getPickedImage() {
        return pickedImage;
    }

    public void saveDraft(Post post){
        if(mLivePost.getValue().getPostPid()==null){// this is draft post
            mDataRepository.getUser(mUserId, new DataSource.OnResponse<User>() {
                @Override
                public void onSuccess(User type) {
                    Post draftPost = mLivePost.getValue();
                    draftPost.setUser(type);
                    mDataRepository.saveDraftPost(draftPost);
                }

                @Override
                public void onFailure(Exception exception) {

                }
            });

        }
    }

    public void clearDraft() {
        mDataRepository.clearDraftPost();
    }
}
