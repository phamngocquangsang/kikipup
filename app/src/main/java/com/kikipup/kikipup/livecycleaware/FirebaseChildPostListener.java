package com.kikipup.kikipup.livecycleaware;

import android.arch.lifecycle.LiveData;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;

public class FirebaseChildPostListener<T> extends LiveData<T>{

    private final ChildEventListener mListener;
    private final DatabaseReference mRef;

    public FirebaseChildPostListener(DatabaseReference ref , ChildEventListener listener) {
        super();
        mRef = ref;
        mListener = listener;
        ref.addChildEventListener(listener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        mRef.removeEventListener(mListener);
    }
}
