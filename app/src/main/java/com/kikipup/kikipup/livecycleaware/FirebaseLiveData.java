package com.kikipup.kikipup.livecycleaware;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.MissingResourceException;

public class FirebaseLiveData<T> extends MutableLiveData<T> {

    private static final String TAG = FirebaseLiveData.class.getSimpleName();
    private final DatabaseReference mReference;
    private final GenericTypeIndicator<T> mType;
    private ValueEventListener mListener;

    public FirebaseLiveData(DatabaseReference ref,@Nullable GenericTypeIndicator<T> type) {
        mReference = ref;
        mType = type;
    }

    public void setListener(ValueEventListener listener) {
        mListener = listener;
    }

    @Override
    protected void onActive() {
        super.onActive();
        if(mListener == null){
            mListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    setValue(dataSnapshot.getValue(mType));
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };
        }
        mReference.addValueEventListener(mListener);
    }

    @Override
    protected void onInactive() {
        Log.i(TAG, "onInactive: ");
        if(mListener!=null){
            mReference.removeEventListener(mListener);
        }
    }

}
