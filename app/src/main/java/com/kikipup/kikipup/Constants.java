package com.kikipup.kikipup;

/**
 * Created by Quang Quang on 7/17/2016.
 */
public final class Constants {

    /**
     * Constants for bundles, extras and shared preferences keys
     */
    public static final String ONE_SIGNAL_APP_ID = "94f3eedc-0d2b-4bdd-b9ac-9432b05140c7";
    public static final String FIRST_TIME_DOWNLOAD = "first_time_download";
    public static final String KIKI_PUP_PREFERENCE = "kikipup_preference";
    public static final String USERLIST_PREFERENCE = "userlist_preference";
    public static final String IS_LOGGED = "icychat_is_logged";
    public static final String LOGGED_USER_JSON = "logged_user_json";
    public static final String SELECTED_SCHOOL = "selected_school";
    //Constants for FIREBASE LOCATION
    public static final String FIREBASE_LOCATION_USERS = "users";
    public static final String LOCATION_POSTS = "posts";
    public static final String LOCATION_USER_POSTS = "userPosts";

    public static final String LOCATION_POST_LIKE = "postLike";
    public static final String LOCATION_POST_DISLIKE = "postDislike";
    public static final String LOCATION_POST_SAD = "postSad";
    public static final String LOCATION_POST_LOVE = "postLove";
    public static final String LOCATION_POST_LAUGH = "postLaugh";

    public static final String LOCATION_USER_LIKE = "userLikes";
    public static final String LOCATION_USER_DISLIKE = "userDislike";
    public static final String LOCATION_USER_LAUGH = "userLaugh";
    public static final String LOCATION_USER_SAD = "userSad";
    public static final String LOCATION_USER_LOVE = "userLove";
    public static final String LOCATION_DRAFT_POSTS = "draftPosts";

    public static final String LOCATION_POST_COMMENTS = "postComments";
    public static final String FIREBASE_LOCATION_POST_SUBCOMMENT = "postSubComments";
    public static final String FIREBASE_LOCATION_USER_COMMENTS = "userComments";
    public static final String FIREBASE_LOCATION_USER_SUBCOMMENT = "userSubComments";
    public static final String FIREBASE_LOCATION_COMMENTS = "comments";
    public static final String FIREBASE_LOCATION_LOGS = "logs";
    public static final String FIREBASE_LOCATION_LOG_APP_OPEN = "logsOpen";
    public static final String FIREBASE_LOCATION_LOG_APP_LEAVE = "logsLeave";
    public static final String FIREBASE_LOCATION_LOG_APP_CANCEL_LOGIN = "logsCancelLogin";
    public static final String FIREBASE_LOCATION_LOG_USER_READ = "logsUserRead";
    public static final String FIREBASE_LOCATION_USER_NOTIFICATIONS = "userNotifications";
    public static final String FIREBASE_LOCATION_FAKE_USERS = "fakeUsers";


    public static final String FIREBASE_LOCATION_SUBCOMMENTS = "subComment";
    public static final String FIREBASE_LOCATION_COMMENT_LIKES = "commentLikes";
    public static final String FIREBASE_LOCATION_COMMENT_DISLIKES = "commentDislikes";
    public static final String FIREBASE_LOCATION_USER_COMMENT_LIKES = "userCommentLikes";
    public static final String FIREBASE_LOCATION_USER_COMMENT_DISLIKES = "userCommentDislikes";
    public static final String FIREBASE_LOCATION_REPORT_POSTS = "reportPosts";
    public static final String FIREBASE_LOCATION_REPORT_COMMENTS = "reportComments";
    public static final String FIREBASE_LOCATION_POST_REPORT_BY = "postReportBy";
    public static final String FIREBASE_LOCATION_COMMENT_REPORT_BY = "commentReportBy";

    //SCHOOL PROPERTY
    public static final String FIREBASE_PROPERTY_SCHOOL_NAME = "schoolName";
    public static final String FIREBASE_PROPERTY_SCHOOL_PID = "schoolPid";
    public static final String FIREBASE_PROPERTY_SCHOOL_ADDRESS = "address";
    public static final String FIREBASE_PROPERTY_SCHOOL_POST_COUNT = "postCount";
    public static final String FIREBASE_PROPERTY_SCHOOL_MEMBER_COUNT = "memberCount";
    public static final String FIREBASE_PROPERTY_SCHOOL_IMAGE_URL = "imageUrl";
    //USER PROPERTY
    public static final String FIREBASE_PROPERTY_USER_ID = "userId";
    public static final String FIREBASE_PROPERTY_USER_EMAIL= "email";
    public static final String FIREBASE_PROPERTY_USER_NAME= "name";
    public static final String FIREBASE_PROPERTY_USER_TIME_JOINED= "timeJoined";
    public static final String FIREBASE_PROPERTY_USER_PROFILE_URL= "profileImageUrl";
    public static final String FIREBASE_PROPERTY_USER_LIKE_COUNT= "likeCount";
    public static final String FIREBASE_PROPERTY_USER_DISLIKE_COUNT = "dislikeCount";
    public static final String FIREBASE_PROPERTY_USER_HELPFUL_COUT= "helpfulCount";
    public static final String FIREBASE_PROPERTY_USER_UNHELPFUL_COUT= "unhelpfulCount";
    public static final String FIREBASE_PROPERTY_USER_IS_ADMIN= "isAdmin";
    public static final String FIREBASE_PROPERTY_USER_SCHOOL= "school";
    public static final String FIREBASE_PROPERTY_USER_INTEREST_CATEGORIES= "interestCategories";
    public static final String FIREBASE_PROPERTY_USER_OS_ID= "OSUserId";
    public static final String FIREBASE_PROPERTY_USER_REGISTRATION_ID= "registrationId";
    //POST PROPERTY
    public static final String FIREBASE_PROPERTY_POST_WRITE_IN= "writtenIn";
    public static final String FIREBASE_PROPERTY_POST_WRITTEN_BY = "user";
    public static final String FIREBASE_PROPERTY_POST_TIME_CREATED = "timeCreated";
    public static final String FIREBASE_PROPERTY_POST_LAST_TIME_EDITED = "lastTimeEdited";
    public static final String FIREBASE_PROPERTY_POST_TITLE = "title";
    public static final String FIREBASE_PROPERTY_POST_COMMENT_COUNT = "commentCount";
    public static final String FIREBASE_PROPERTY_POST_READ_COUNT = "readCount";
    public static final String FIREBASE_PROPERTY_POST_TIMESTAMP = "timestamp";
    public static final String FIREBASE_PROPERTY_POST_POST_ID = "postPid";
    public static final String FIREBASE_PROPERTY_POST_MENTION_PEOPLE = "mentions";
    public static final String FIREBASE_PROPERTY_POST_CATEGORY = "category";
    public static final String FIREBASE_PROPERTY_POST_REACTION = "reactions";
    //COMMENT PROPERTY
    public static final String FIREBASE_PROPERTY_COMMENT_PID= "commentPid";
    public static final String FIREBASE_PROPERTY_COMMENT_TIME_CREATED= "timeCreated";
    public static final String FIREBASE_PROPERTY_COMMENT_LAST_TIME_EDITED= "lastTimeEdited";
    public static final String FIREBASE_PROPERTY_COMMENT_CONTENT= "content";
    public static final String FIREBASE_PROPERTY_COMMENT_POST_PID= "postPid";
    public static final String FIREBASE_PROPERTY_COMMENT_WRITTENBY= "writtenBy";
    public static final String FIREBASE_PROPERTY_COMMENT_HELPFUL_COUNT= "helpfulCount";
    public static final String FIREBASE_PROPERTY_COMMENT_UNHELPFUL_COUNT= "unhelpfulCount";
    public static final String FIREBASE_PROPERTY_COMMENT_SUBCOMMENT= "subComment";
    public static final String FIREBASE_PROPERTY_COMMENT_LAST_COMMENT= "lastComment";
    public static final String FIREBASE_PROPERTY_COMMENT_TIMESTAMP = "timestamp";

    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_PID = "notificationPid";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_TIMESTAMP = "timestamp";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_TYPE = "type";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_FROM_USER = "fromUser";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_COMMENT_CONTENT = "commentContent";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_TIME_CREATED = "timeCreated";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_SEEN = "seen";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_LIKED_POST = "likedPost";
    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_LIKED_COMMENT = "likedComment";

    public static final String FIREBASE_PROPERTY_USER_NOTIFICATIONS_USER_EMAIL = "userEmail";



    public static final String ANONYMOUS_EMAIL = "anonymou@com.kikipup.kikipup.com" ;
    public static final String ANONYMOUS_EMAIL_DOMAIN = "com.kikipup.kikipup.com";
    public static final String RANDOM_USER_EMAIL = "random@com.kikipup.kikipup.com" ;
    public static final String FIREBASE_PROPERTY_POST_TITLE_IMAGE_URL = "titleImageUrl" ;

    public static final String FIREBASE_PROPERTY_POST_MEDIA = "media";
    public static final String FIREBASE_PROPERTY_POST_CONTENT = "content";

}
