package com.kikipup.kikipup.fragments;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kikipup.kikipup.R;
import com.kikipup.kikipup.activities.ComposerActivity;
import com.kikipup.kikipup.activities.PostDetailActivity;
import com.kikipup.kikipup.activities.ScrollingActivity;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;
import com.kikipup.kikipup.viewmodel.KikiViewModel;
import com.kikipup.kikipup.viewmodel.PostListViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnPostListInteractListener}
 * interface.
 */
public class PostFragment extends LifecycleFragment {

    private static final String TAG = PostFragment.class.getSimpleName();
    private static String ARG_USER_ID = "arg-user-id";

    private OnPostListInteractListener mListener;

    private PostAdapter mAdapter;

    private PostListViewModel mPostListViewModel;

    private String mUserId;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PostFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PostFragment newInstance(String userId) {
        PostFragment fragment = new PostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_list, container, false);
        mListener = new OnPostListInteractListener() {
            @Override
            public void onPostClicked(Post item) {
                //todo open post detail
                Intent i = PostDetailActivity.newIntent(getActivity(), item.getPostPid());
//                Intent i = new Intent(getActivity(), ScrollingActivity.class);
                startActivity(i);
            }

            @Override
            public void onLike(Post item) {
                Log.i(TAG, "onLike: postPid: "+item.getPostPid());
                mPostListViewModel.likePost(mUserId, item.getPostPid());
            }

            @Override
            public void onDislike(Post item) {
                Log.i(TAG, "onDislike: ");
                mPostListViewModel.dislikePost(mUserId, item.getPostPid());
            }

            @Override
            public void onSad(Post item) {
                Log.i(TAG, "onSad: ");
                mPostListViewModel.sadPost(mUserId, item.getPostPid());
            }

            @Override
            public void onLaugh(Post item) {
                Log.i(TAG, "onLaugh: ");
                mPostListViewModel.laughPost(mUserId, item.getPostPid());
            }

            @Override
            public void onLove(Post item) {
                Log.i(TAG, "onLove: ");
                mPostListViewModel.lovePost(mUserId, item.getPostPid());
            }

            @Override
            public void onComment(Post item) {

            }

            @Override
            public void onMenu(Post item) {
                openPostMenu(item);
            }

            @Override
            public void onProfile(User profile) {

            }
        };
        mUserId = savedInstanceState == null ? getArguments().getString(ARG_USER_ID) : savedInstanceState.getString(ARG_USER_ID);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            if(mListener == null){
                throw new IllegalStateException("mListener is null, have you call setListener() after create Fragment instance, " +
                        "please prefer using newInstance() instead initialize manually");
            }
            mAdapter = new PostAdapter(new ArrayList<Post>(), mListener);
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    private void openPostMenu(final Post item) {
        ArrayList<String> options = new ArrayList<>();
        options.add(getString(R.string.post_option_edit));
        options.add(getString(R.string.post_option_delete));
        options.add(getString(R.string.post_option_report));
        PostOptionListDialogFragment optionListDialog = PostOptionListDialogFragment.newInstance(options);
        optionListDialog.show(getChildFragmentManager(), PostOptionListDialogFragment.class.getSimpleName());
        optionListDialog.setOnOptionSelectListener(new PostOptionListDialogFragment.Listener() {
            @Override
            public void onPostOptionClicked(int position, String option) {
                final String edit = getString(R.string.post_option_edit);
                final String delete = getString(R.string.post_option_delete);
                final String report = getString(R.string.post_option_report);
                if(option.equals(edit)){
                    startActivity(ComposerActivity.newIntent(getActivity(), mUserId, item.getPostPid()));
                }else if(option.equals(delete)){
                    deletePost(item);
                } else if (option.equals(report)) {
                    //todo handle report post
                }
            }
        });
    }

    private void deletePost(Post item) {
        if(mUserId.equals(item.getUser().getUserId())){
            mPostListViewModel.deletePost(item.getPostPid());
        }else{
            Toast.makeText(getContext(), "you can not delete this post", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_USER_ID, mUserId);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        mPostListViewModel = ViewModelProviders.of(this).get(PostListViewModel.class);

        mPostListViewModel.initializeUserId(mUserId);

        mPostListViewModel.getUserLikePosts().observe(this, new Observer<Map<String, Boolean>>() {
            @Override
            public void onChanged(@Nullable Map<String, Boolean> userLikePosts) {
                setUserLikePosts(userLikePosts);
            }
        });

        mPostListViewModel.getUserDislikePosts().observe(this, new Observer<Map<String, Boolean>>() {
            @Override
            public void onChanged(@Nullable Map<String, Boolean> stringBooleanMap) {
                setUserDisLikePosts(stringBooleanMap);
            }
        });

        mPostListViewModel.getUserLaughPosts().observe(this, new Observer<Map<String, Boolean>>() {
            @Override
            public void onChanged(@Nullable Map<String, Boolean> stringBooleanMap) {
                setUserLaughPosts(stringBooleanMap);
            }
        });

        mPostListViewModel.getUserLovePosts().observe(this, new Observer<Map<String, Boolean>>() {
            @Override
            public void onChanged(@Nullable Map<String, Boolean> stringBooleanMap) {
                setUserLovePosts(stringBooleanMap);
            }
        });

        mPostListViewModel.getUserSadPosts().observe(this, new Observer<Map<String, Boolean>>() {
            @Override
            public void onChanged(@Nullable Map<String, Boolean> stringBooleanMap) {
                setUserSadPosts(stringBooleanMap);
            }
        });
    }

    public interface OnPostListInteractListener {
        // TODO: Update argument type and name
        void onPostClicked(Post item);

        void onLike(Post item);

        void onDislike(Post item);

        void onSad(Post item);

        void onLaugh(Post item);

        void onLove(Post item);

        void onComment(Post item);

        void onMenu(Post item);

        void onProfile(User profile);
    }

    public void setData(List<Post> list){
        mAdapter.setValues(list);
    }

    public void setListener(OnPostListInteractListener listener){
        mListener = listener;
    }

    public void setUserLikePosts(Map<String, Boolean> likePosts){
        mAdapter.setUserLikePosts(likePosts);
    }

    public void setUserDisLikePosts(Map<String, Boolean> disLikePosts){
        mAdapter.setUserDisLikePosts(disLikePosts);
    }

    public void setUserLovePosts(Map<String, Boolean> lovePosts){
        mAdapter.setUserLovePosts(lovePosts);
    }

    public void setUserSadPosts(Map<String, Boolean> sadPosts){
        mAdapter.setUserSadPosts(sadPosts);
    }

    public void setUserLaughPosts(Map<String, Boolean> laughPosts){
        mAdapter.setUserLaughPosts(laughPosts);
    }

}
