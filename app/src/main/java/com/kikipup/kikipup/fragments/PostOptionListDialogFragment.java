package com.kikipup.kikipup.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.formats.MediaView;
import com.kikipup.kikipup.R;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     PostOptionListDialogFragment.newInstance(30).show(getSupportFragmentManager(), "dialog");
 * </pre>
 * <p>You activity (or fragment) needs to implement {@link PostOptionListDialogFragment.Listener}.</p>
 */
public class PostOptionListDialogFragment extends BottomSheetDialogFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_ALL_OPTION = "all-option";
    private Listener mListener;

    // TODO: Customize parameters
    public static PostOptionListDialogFragment newInstance(ArrayList<String> allOptions) {
        final PostOptionListDialogFragment fragment = new PostOptionListDialogFragment();
        final Bundle args = new Bundle();
        args.putStringArrayList(ARG_ALL_OPTION, allOptions);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_post_list_bottom_sheet, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new PostOptionAdapter(getArguments().getStringArrayList(ARG_ALL_OPTION)));
    }


    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    public interface Listener {
        void onPostOptionClicked(int position, String option);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView text;

        String value;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.post_option_item, parent, false));
            text = (TextView) itemView.findViewById(R.id.text);
            text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onPostOptionClicked(getAdapterPosition(), value);
                        dismiss();
                    }
                }
            });
        }

    }

    private class PostOptionAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final ArrayList<String> mValues;

        PostOptionAdapter(ArrayList<String> itemCount) {
            mValues = itemCount;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.text.setText(mValues.get(position));
            holder.value = mValues.get(position);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

    }


    public void setOnOptionSelectListener(Listener listener){
        mListener = listener;
    }
}
