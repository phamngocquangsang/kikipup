package com.kikipup.kikipup.fragments;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ProgressBar;

import com.kikipup.kikipup.R;
import com.kikipup.kikipup.databinding.PollAnswerLayoutBinding;
import com.kikipup.kikipup.fragments.PollAnswerFragment.OnUserAnswerPolls;
import com.kikipup.kikipup.fragments.dummy.DummyContent.DummyItem;
import com.kikipup.kikipup.models.Question;
import com.kikipup.kikipup.models.QuestionOption;

import java.nio.MappedByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnUserAnswerPolls}.
 * TODO: Replace the implementation with code for your data type.
 */
public class PollAnswerAdapter extends RecyclerView.Adapter<PollAnswerAdapter.ViewHolder> {

    private final Question mQuestion;
    private final PollFragment.PollListInteractionListener mListener;

    public PollAnswerAdapter(Question question, PollFragment.PollListInteractionListener listener) {
        mQuestion = question;
        mListener = listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.poll_answer_layout, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mQuestion.getQuestionOptions().get(position);
        holder.mBinding.tvAnswer.setText(holder.mItem.getTitle());
        if(mQuestion.isShownResults()){
            //update UI for shown result
            holder.mBinding.checkbox.setVisibility(View.GONE);
            holder.mBinding.tvVoteResult.setVisibility(View.VISIBLE);

            animateProgress(holder.mBinding.progressBar,
                    0,
                    (int) (holder.mItem.getVoteResult() * holder.mBinding.progressBar.getMax()));

            holder.mBinding.tvVoteResult.setText(holder.mItem.getVoteResult() * 100+ "%");
            if((mQuestion.getAnswers().contains(holder.mItem.getId()))){
                Drawable activeProgressDrawable =
                        holder.mBinding.getRoot().getContext().getDrawable(R.drawable.poll_progress_bar_active);
                holder.mBinding.progressBar
                        .setProgressDrawable(activeProgressDrawable);
            }else{
                Drawable activeProgressDrawable =
                        holder.mBinding.getRoot().getContext().getDrawable(R.drawable.poll_progress_bar);
                holder.mBinding.progressBar
                        .setProgressDrawable(activeProgressDrawable);
            }
        }else{
            holder.mBinding.checkbox.setVisibility(View.VISIBLE);
            holder.mBinding.tvVoteResult.setVisibility(View.GONE);
            holder.mBinding.progressBar.setProgress(0);
        }

        holder.mBinding.checkbox.setChecked(mQuestion.getAnswers().contains(holder.mItem.getId()));

        holder.mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mQuestion.isShownResults()){
                    return;
                }
                List<String> answer = mQuestion.getAnswers();
                if(answer.contains(holder.mItem.getId())){
                    answer.remove(holder.mItem.getId());
                }else{
                    if(!mQuestion.isMultipleResponse()){
                        answer.clear();
                    }
                    answer.add(holder.mItem.getId());
                }
                mQuestion.setAnswers(answer);
                notifyItemRangeChanged(0, mQuestion.getQuestionOptions().size());
            }
        });
    }

    private void animateProgress(ProgressBar progressBar, int fromValue, int toValue) {
        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressBar, "progress",
                fromValue, toValue);
        progressAnimator.setDuration(300);
        progressAnimator.setInterpolator(new DecelerateInterpolator());
        progressAnimator.start();
    }

    @Override
    public int getItemCount() {
        return mQuestion.getQuestionOptions().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private final PollAnswerLayoutBinding mBinding;
        public QuestionOption mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mBinding = DataBindingUtil.bind(view);
        }
    }
}
