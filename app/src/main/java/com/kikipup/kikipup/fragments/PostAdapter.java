package com.kikipup.kikipup.fragments;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kikipup.kikipup.BR;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.fragments.PostFragment.OnPostListInteractListener;
import com.kikipup.kikipup.models.Post;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.validation.Validator;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private final ArrayList<Post> mValues;

    private Map<String, Boolean> mUserLikePosts = new HashMap<>();

    private Map<String, Boolean> mUserDisLikePosts = new HashMap<>();

    private Map<String, Boolean> mUserLovePosts = new HashMap<>();

    private Map<String, Boolean> mUserSadPosts = new HashMap<>();

    private Map<String, Boolean> mUserLaughPosts = new HashMap<>();

    private OnPostListInteractListener mListener;

    public PostAdapter(ArrayList<Post> items, OnPostListInteractListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_post, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mBinding.setVariable(BR.callback, mListener);
        holder.mItem = mValues.get(position);

        holder.mBinding.setVariable(BR.post, mValues.get(position));
        holder.mBinding.setVariable(BR.likePosts, mUserLikePosts);
        holder.mBinding.setVariable(BR.dislikePosts, mUserDisLikePosts);
        holder.mBinding.setVariable(BR.lovePosts, mUserLovePosts);
        holder.mBinding.setVariable(BR.sadPosts, mUserSadPosts);
        holder.mBinding.setVariable(BR.laughPosts, mUserLaughPosts);


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setListener(OnPostListInteractListener listener) {
        mListener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public Post mItem;
        private final ViewDataBinding mBinding;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mBinding = DataBindingUtil.bind(view);
        }

    }

    public void setValues(List<Post> list){
        mValues.clear();
        mValues.addAll(list);
        notifyDataSetChanged();
    }

    public void setUserLikePosts(@Nullable Map<String, Boolean> likePosts){
        mUserLikePosts = likePosts;
        notifyDataSetChanged();
    }

    public void setUserDisLikePosts(@Nullable Map<String, Boolean> disLikePosts){
        mUserDisLikePosts = disLikePosts;
        notifyDataSetChanged();
    }

    public void setUserLovePosts(@Nullable Map<String, Boolean> lovePosts){
        mUserLovePosts = lovePosts;
        notifyDataSetChanged();
    }

    public void setUserSadPosts(@Nullable Map<String, Boolean> sadPosts){
        mUserSadPosts = sadPosts;
        notifyDataSetChanged();
    }

    public void setUserLaughPosts(@Nullable Map<String, Boolean> laughPosts){
        mUserLaughPosts = laughPosts;
        notifyDataSetChanged();
    }
}
