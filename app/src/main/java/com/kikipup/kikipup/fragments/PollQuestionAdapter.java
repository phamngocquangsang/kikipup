package com.kikipup.kikipup.fragments;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kikipup.kikipup.R;
import com.kikipup.kikipup.databinding.FragmentPollBinding;
import com.kikipup.kikipup.fragments.PollFragment.PollListInteractionListener;
import com.kikipup.kikipup.fragments.dummy.DummyContent;
import com.kikipup.kikipup.fragments.dummy.DummyContent.DummyItem;
import com.kikipup.kikipup.models.Question;

import java.nio.MappedByteBuffer;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link PollListInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class PollQuestionAdapter extends RecyclerView.Adapter<PollQuestionAdapter.ViewHolder> {

    private final List<Question> mValues;
    private final PollListInteractionListener mListener;

    public PollQuestionAdapter(List<Question> items, PollListInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_poll, parent, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.answerList);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.mBinding.tvQuestion.setText(holder.mItem.getTitle());
        holder.mBinding.answerList.setAdapter(new PollAnswerAdapter(holder.mItem, mListener));

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final FragmentPollBinding mBinding;
        public Question mItem;

        public ViewHolder(View view) {
            super(view);
            mBinding = DataBindingUtil.bind(view);
        }

    }
}
