package com.kikipup.kikipup.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.gesture.Gesture;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.kikipup.kikipup.R;
import com.kikipup.kikipup.databinding.FragmentPollListBinding;
import com.kikipup.kikipup.models.LinearLayoutManagerScrollDisabled;
import com.kikipup.kikipup.models.Poll;
import com.kikipup.kikipup.models.Question;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link PollListInteractionListener}
 * interface.
 */
public class PollFragment extends Fragment {

    private static final String ARG_POLL = "arg-poll";
    private static final String TAG = PollFragment.class.getSimpleName();
    private PollListInteractionListener mListener;
    private LinearLayoutManagerScrollDisabled mLayoutManager;
    private FragmentPollListBinding mBinding;
    private Poll mPoll;
    private PollQuestionAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PollFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PollFragment newInstance(Poll poll) {
        PollFragment fragment = new PollFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_POLL, poll);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mPoll = getArguments().getParcelable(ARG_POLL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_poll_list, container, false);

        // Set the adapter
        mLayoutManager = new LinearLayoutManagerScrollDisabled(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager.setScrollable(false);

        mBinding.list.setLayoutManager(mLayoutManager);
        mAdapter = new PollQuestionAdapter(mPoll.getQuestions(), mListener);
        mBinding.list.setAdapter(mAdapter);
        mBinding.list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    mLayoutManager.setScrollable(false);
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });


        return mBinding.getRoot();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PollListInteractionListener) {
            mListener = (PollListInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement PollListInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * get Current Page
     * @return current page, Zero-based index
     */
    public int getCurrentPage() {
        if(mLayoutManager!=null){
            return mLayoutManager.findFirstVisibleItemPosition();
        }else{
            return 0;
        }
    }

    public void showVoteResult() {
        Question question = mPoll.getQuestions().get(getCurrentPage());
        question.setShownResults(true);
        mAdapter.notifyItemChanged(getCurrentPage());
    }

    public Question getCurrentQuestion() {
        return mPoll.getQuestions().get(getCurrentPage());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface PollListInteractionListener {

        void onPageScroll(int currentPage, Question question);

    }

    public void scrollNext(){
        mLayoutManager.setScrollable(true);
        int currentPosition = mLayoutManager.findFirstVisibleItemPosition() + 1;
        if(currentPosition > mPoll.getQuestions().size() - 1){
            return;
        }
        mLayoutManager.smoothScrollToPosition(mBinding.list, null, currentPosition);
        mListener.onPageScroll(currentPosition, mPoll.getQuestions().get(currentPosition));
    }

    public void scrollBack(){
        int currentPosition = mLayoutManager.findFirstVisibleItemPosition() - 1;
        if(currentPosition < 0){
            return;
        }
        mLayoutManager.setScrollable(true);
        mLayoutManager.smoothScrollToPosition(mBinding.list, null, currentPosition);
        mListener.onPageScroll(currentPosition, mPoll.getQuestions().get(currentPosition));
    }

    public Poll getPoll() {
        return mPoll;
    }
}
