package com.kikipup.kikipup.activities;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.formats.MediaView;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.Utilities;
import com.kikipup.kikipup.databinding.ActivityPostDetailBinding;
import com.kikipup.kikipup.fragments.CommentFragment;
import com.kikipup.kikipup.fragments.dummy.DummyContent;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.viewmodel.PostDetailViewModel;

public class PostDetailActivity extends AppCompatActivity implements LifecycleRegistryOwner,
        CommentFragment.OnListFragmentInteractionListener {

    private LifecycleRegistry mLifecycle = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle() {
        return mLifecycle;
    }

    public static final String TAG = PostDetailActivity.class.getSimpleName();

    private static final String ARG_POST_ID = "arg-post-id";

    private ActivityPostDetailBinding mBinding;

    private PostDetailViewModel mViewModel;

    public static Intent newIntent(Context c, String postId){
        Intent i = new Intent(c, PostDetailActivity.class);
        i.putExtra(ARG_POST_ID, postId);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_post_detail);

        mViewModel = ViewModelProviders.of(this).get(PostDetailViewModel.class);

        mViewModel.loadPost(getIntent().getStringExtra(ARG_POST_ID)).observe(this, new Observer<Post>() {
            @Override
            public void onChanged(@Nullable Post post) {
                if(post!=null){
                    mBinding.setPost(post);
                }
            }
        });

//        CommentFragment fragment = CommentFragment.newInstance(1);
//        getSupportFragmentManager()
//                .beginTransaction().replace(R.id.fragmentContainer, fragment).commit();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mBinding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, final int verticalOffset) {
                Log.i(TAG, "onOffsetChanged: " +verticalOffset);
                if(verticalOffset == 0 || verticalOffset <= mBinding.toolbar.getHeight()){
                    Log.i(TAG, "onOffsetChanged: collapsed");
                    mBinding.toolbar.setTitle("mCollapsedTitle");
                }else {
                    Log.i(TAG, "onOffsetChanged: expanded");
                    mBinding.toolbar.setTitle("mExpandedTitle");
                }

            }
        });


    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }
}
