package com.kikipup.kikipup.activities;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.kikipup.kikipup.R;
import com.kikipup.kikipup.databinding.ActivityPollBinding;
import com.kikipup.kikipup.fragments.PollFragment;
import com.kikipup.kikipup.models.Poll;
import com.kikipup.kikipup.models.Question;

public class PollActivity extends AppCompatActivity
        implements PollFragment.PollListInteractionListener,
                    View.OnClickListener{

    private PollFragment mFragment;
    private ActivityPollBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_poll);

        Poll poll = Poll.sDummyPoll;

        mBinding.tvCurrentPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragment.scrollNext();
            }
        });

        mFragment = PollFragment.newInstance(poll);
        getSupportFragmentManager().beginTransaction().add(R.id.contentLayout, mFragment).commit();
        updateProgress(poll, 0);

        mBinding.btnBack.setOnClickListener(this);
        mBinding.btnVote.setOnClickListener(this);

    }

    @SuppressLint("SetTextI18n")
    /***
     * current page is zero-based index
     */
    private void updateProgress(Poll poll, int currentPage) {
        int current = currentPage + 1;
        int totalQuestion = poll.getQuestions().size();
        if(totalQuestion == 0){
            return;
        }
        mBinding.tvCurrentPage.setText(current + "/" + totalQuestion);

        //        mBinding.progressBar.setProgress((int) (current / (float)totalQuestion *100));

        int progressTo = (int) (current / (float)totalQuestion * mBinding.progressBar.getMax());

        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(mBinding.progressBar, "progress",
                mBinding.progressBar.getProgress(), progressTo);
        progressAnimator.setDuration(300);
        progressAnimator.setInterpolator(new DecelerateInterpolator());
        progressAnimator.start();
    }



    @Override
    public void onPageScroll(int currentPage, Question question) {
        updateProgress(mFragment.getPoll(), currentPage);
        if(question.isShownResults()){
            mBinding.btnVote.setText("Continue");
        }else{
            mBinding.btnVote.setText("Vote");
        }
    }

    @Override
    public void onClick(View v) {
        if(v == mBinding.btnVote){
            onVote();

        }else if(v == mBinding.btnBack){
            if(mFragment.getCurrentPage() == 0){
                finish();
                return;
            }
            mFragment.scrollBack();
        }
    }

    private void onVote() {
        Question current = mFragment.getCurrentQuestion();
        if(current.isMandatory() && current.getAnswers().isEmpty()){
            Toast.makeText(this, "you haven't answer yet!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!current.isShownResults()){
            mFragment.showVoteResult();
        }else{
            mFragment.scrollNext();
        }
    }

}
