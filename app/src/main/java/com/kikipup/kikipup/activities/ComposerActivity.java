package com.kikipup.kikipup.activities;

import android.Manifest;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.formats.MediaView;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.databinding.ActivityComposerBinding;
import com.kikipup.kikipup.models.Media;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.Reaction;
import com.kikipup.kikipup.viewmodel.ComposerViewModel;

public class ComposerActivity extends AppCompatActivity
        implements View.OnClickListener, LifecycleRegistryOwner {

    private LifecycleRegistry mLifeCycle = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle() {
        return mLifeCycle;
    }

    private static final String TAG = ComposerActivity.class.getSimpleName();

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    private static final int REQUEST_CODE_PICK_PHOTO = 2;

    private static String ARG_POST_ID = "arg-post-id";

    private static String ARG_USER_ID = "arg-user-id";

    private ActivityComposerBinding mBinding;

    private String mUserId;

    private String mPostId;

    private ComposerViewModel mViewModel;

    /**
     * @param loggedUserId id of logged user
     * @param postId pass null if create new post else update existing post
     * @return Intent that has param set up
     */
    public static Intent newIntent(Context c, @NonNull String loggedUserId, @Nullable String postId){
        Intent i = new Intent(c, ComposerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(ARG_USER_ID, loggedUserId);
        bundle.putString(ARG_POST_ID, postId);
        i.putExtras(bundle);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserId = getIntent().getExtras().getString(ARG_USER_ID);
        mPostId = getIntent().getExtras().getString(ARG_POST_ID);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_composer);
        setSupportActionBar(mBinding.toolbar);

        mViewModel = ViewModelProviders.of(this).get(ComposerViewModel.class);
        mViewModel.initializeUserId(mUserId);

        mViewModel.initializePostId(mPostId);
        mViewModel.getLivePost().observe(this, new Observer<Post>() {
            @Override
            public void onChanged(@Nullable Post post) {
                if(post!=null){
                    mBinding.setPost(post);
                }
            }
        });

//        mViewModel.getPickedImage().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String pickedImage) {
//                if(pickedImage!=null){
//                    mBinding.content.deleteImageBtn.setVisibility(View.VISIBLE);
//                    mBinding.content.imageContent.setVisibility(View.VISIBLE);
//                    Glide.with(ComposerActivity.this).load(pickedImage).into(mBinding.content.imageContent);
//                }else{
//                    if(mBinding.getPost().)
//                }
//            }
//        });

        setUpListener(mBinding);
    }

    private void setUpListener(ActivityComposerBinding binding) {
        binding.content.toolbar.imageViewEditorTakeImage.setOnClickListener(this);
        binding.content.deleteImageBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView_editor_take_image:
                requestStoragePermission();
                break;
            case R.id.delete_image_btn:
                clearImage();
                break;
        }
    }

    private void clearImage() {
        Post newPost = mViewModel.getLivePost().getValue();
        newPost.setMedia(null);
        mViewModel.getLivePost().setValue(newPost);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_composer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_compose_post:
                Log.i(TAG, "onOptionsItemSelected: ");
                createOrUpdatePost();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void createOrUpdatePost() {
        if(mPostId != null){
            Post updatedPost = mBinding.getPost();
            updatedPost.setTitle(mBinding.content.textViewTittle.getText().toString());
            updatedPost.setContent(mBinding.content.textViewContent.getText().toString());
            mViewModel.updatePost(updatedPost);
            Log.i(TAG, "update post");
        }else{
            Log.i(TAG, "createOrUpdatePost: ");
           Post post = mBinding.getPost();
           post.setTitle(mBinding.content.textViewTittle.getText().toString());
           post.setContent(mBinding.content.textViewContent.getText().toString());
           mViewModel.createPost(post, mUserId);
        }
        mViewModel.clearDraft();
        finish();
    }

    void requestStoragePermission(){
        int permission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (PackageManager.PERMISSION_GRANTED != permission) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            //TODO start image picker
            startImagePicker();
        }
    }
    public void startImagePicker() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_PICK_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_PICK_PHOTO: {
                if (resultCode == RESULT_OK) {
//                    mViewModel.setPickedImage(data.getData().toString());
//                    Log.i(TAG, "onActivityResult: user selected image path: " + mPickedImage);
                    Post post = mViewModel.getLivePost().getValue();
                    Media media = new Media(Media.TYPE_IMAGE, data.getData().toString(), 0 , 0);
                    post.setMedia(media);
                    mViewModel.getLivePost().setValue(post);
                }
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    startImagePicker();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    Snackbar.make(mBinding.getRoot(),
                            "This permission is required to pick photos",
                            Snackbar.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onStop() {
        Post post = mViewModel.getLivePost().getValue();
        post.setTitle(mBinding.content.textViewTittle.getText().toString());
        post.setContent(mBinding.content.textViewContent.getText().toString());
        mViewModel.saveDraft(post);
        super.onStop();
    }
}
