package com.kikipup.kikipup.activities;

import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.Utilities;
import com.kikipup.kikipup.databinding.ActivityScrollingBinding;

import jp.wasabeef.glide.transformations.BlurTransformation;

public class ScrollingActivity extends AppCompatActivity {

    private static final String TAG = ScrollingActivity.class.getSimpleName();
    private ActivityScrollingBinding mBinding;

    private boolean isToolbarTitleVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_scrolling);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mBinding.fabAppBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        mBinding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.i(TAG, "onOffsetChanged: offset= "+verticalOffset);

                int height = appBarLayout.getTotalScrollRange();
                int toolbarOffset = height - Math.abs(verticalOffset) + mBinding.toolbar.getHeight();
                float percent = Math.abs(verticalOffset)/(float)height;
                Log.i(TAG, "onOffsetChanged: verticalOffset" + verticalOffset+ "height: " + height+" percent alpha " +percent);
                crossfade(mBinding.blurImageEffect, mBinding.originalImage, percent);
                if(toolbarOffset <= mBinding.toolbar.getHeight()){
                    Log.i(TAG, "onOffsetChanged: collapsing - toolbarOffset = "+toolbarOffset);
                    mBinding.fabToolbar.show();
                    mBinding.fabAppBar.hide();

                }else{
                    Log.i(TAG, "onOffsetChanged: expanding "+ toolbarOffset);
                    mBinding.fabToolbar.hide();
                    mBinding.fabAppBar.show();
                }
            }
        });

        mBinding.content.scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView nestedScrollView, int i, int i1, int i2, int i3) {
                Rect scrollBounds = new Rect();
                mBinding.tvTitle.setText("This is title");
                mBinding.content.scrollView.getHitRect(scrollBounds);
                if (mBinding.content.tvName.getLocalVisibleRect(scrollBounds)) {
                    // if layout even a single pixel, is within the visible area do something
//                    mBinding.tvTitle.setText("");
                    hideDownTitle(mBinding.tvTitle);
                    isToolbarTitleVisible = false;
                } else {
                    // if layout goes out or scrolled out of the visible area do something
                    showUpTitle(mBinding.tvTitle);
                    isToolbarTitleVisible = true;
                }

            }
        });

        Glide.with(this).load(R.drawable.dog_example).into(mBinding.originalImage);
        Glide.with(this)
                .load(R.drawable.dog_example)
                .bitmapTransform(new CenterCrop(this), new BlurTransformation(this, 100))
                .crossFade()
                .into(mBinding.blurImageEffect);
        mBinding.blurImageEffect.setVisibility(View.VISIBLE);
    }


    public void showUpTitle(View titleView){
        if(isToolbarTitleVisible){
            return;
        }
        if(titleView.getAnimation()!=null){
            titleView.getAnimation().cancel();
        }
        titleView.animate().translationY(0).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(250).start();
        titleView.animate().alpha(1f).setDuration(150).start();
    }

    public void hideDownTitle(View titleView){
        if(!isToolbarTitleVisible){
            return;
        }
        if(titleView.getAnimation()!=null){
            titleView.getAnimation().cancel();
        }
        titleView.animate().translationY(120).setInterpolator(new DecelerateInterpolator()).setDuration(250).start();
        titleView.animate().alpha(0f).setDuration(150).start();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }

    private void crossfade(View mContentView, final View mLoadingView, float alpha) {
        mContentView.setAlpha(alpha);
        mLoadingView.setAlpha(1 - alpha);
    }
}
