package com.kikipup.kikipup.activities;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.kikipup.kikipup.R;
import com.kikipup.kikipup.databinding.ActivityTrendingBinding;
import com.kikipup.kikipup.fragments.PostFragment;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;
import com.kikipup.kikipup.viewmodel.TrendingViewModel;

import java.util.List;

public class TrendingActivity extends AppCompatActivity implements LifecycleRegistryOwner{

    private LifecycleRegistry mLifecycle = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle() {
        return mLifecycle;
    }

    private static String ARG_USER_ID = "arg-user-id";
    private ActivityTrendingBinding mBinding;
    private PostFragment mFragment;
    private TrendingViewModel mViewModel;
    private String mUserId;

    public static Intent getIntent(Context c, String userId){
        Intent i = new Intent(c, TrendingActivity.class);
        i.putExtra(ARG_USER_ID, userId);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_trending);

        setSupportActionBar(mBinding.toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.trending);

        mUserId = getIntent().getStringExtra(ARG_USER_ID);

        mFragment = PostFragment.newInstance(mUserId);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, mFragment).commit();

        mViewModel = ViewModelProviders.of(this).get(TrendingViewModel.class);

        mViewModel.initializeUserId(mUserId);

        mViewModel.getFakeListPost().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> posts) {
                mFragment.setData(posts);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
