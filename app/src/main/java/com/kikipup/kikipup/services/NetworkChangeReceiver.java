package com.kikipup.kikipup.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.kikipup.kikipup.Constants;
import com.kikipup.kikipup.Utilities;
import com.kikipup.kikipup.models.User;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Quang Quang on 9/17/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    private static final String TAG = NetworkChangeReceiver.class.getSimpleName();


    @Override
    public void onReceive(final Context context, final Intent intent) {

//        Log.e(TAG, "on network reciever");
//        Log.d("app", "Network connectivity change");
//        if (intent.getExtras() != null) {
//            NetworkInfo ni = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
//            if (ni != null
//                    && ni.getState() == NetworkInfo.State.CONNECTED
//                    && ni.getType() == ConnectivityManager.TYPE_WIFI) {
//                Log.i("app", "Network " + ni.getTypeName() + " connected");
//                CommentsOfflineQueue.pushComments(context);
//                User loggedUser = Utilities.getLoggedUser(context);
//                //todo this code for downoad latest data in background when network available.
//                updateNewsfeed(context);//get the latest stories in background.
//                if (!loggedUser.isTemporaryUser()) {
//                    NotificationFragment.loadingUnreadNotificationNumber(loggedUser, new NotificationFragment.OnNotificationInteractionListener() {
//                        @Override
//                        public void onNotificationInteraction(int unreadNotificationCount) {
//                            Log.i(TAG, "onNotificationInteraction: notification updated in background "
//                                    + unreadNotificationCount + " unread noti");
//                        }
//                    });
////                }
//                }
//            }
//            if (intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
//                Log.d("app", "There's no network connectivity");
//            }
//        }
    }

    public void updateNewsfeed(final Context c) {
//        Query query = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_SCHOOL_POSTS).child("4")
//                .orderByKey().limitToLast(NewsfeedFragment.LOADING_POST_STEP);
//        query.keepSynced(true);
//        ValueEventListener listener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                ArrayList<Post> posts = new ArrayList<>();
//                for (DataSnapshot post :
//                        dataSnapshot.getChildren()) {
//                    Post postObject = post.getValue(Post.class);
//                    if (postObject != null) {
//                        posts.add(postObject);
//                    }
//                }
//                Collections.sort(posts,Post.getTimeCreatedCompator());
//                Utilities.savingNewsfeed(c, posts);
//                Log.i(TAG, "finish updateNewsfeed in background: " + posts.size() + " posts in total");
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        };
//        query.addListenerForSingleValueEvent(listener);
    }
}