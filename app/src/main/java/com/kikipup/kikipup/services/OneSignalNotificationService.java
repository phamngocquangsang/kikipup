package com.kikipup.kikipup.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.Utilities;
import com.kikipup.kikipup.models.User;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

/**
 * Created by Quang Quang on 8/25/2016.
 */
public class OneSignalNotificationService extends NotificationExtenderService{
    private static final String TAG = OneSignalNotificationService.class.getSimpleName();
    private static final int NOTIFICATION_LIMITED = 5;

    @Override
    protected boolean onNotificationProcessing(final OSNotificationReceivedResult notification) {
        // Read properties from result.


//        final User loggedUser = Utilities.getLoggedUser(this);
//        if(loggedUser== null || loggedUser.isTemporaryUser()){
//            Log.i(TAG, "onNotificationProcessing: user is sign out or clear data");
//            return true;
//        }
//
//        Gson gson = new Gson();
//        Notification extraData = gson.fromJson(notification.payload.additionalData.toString(), Notification.class);
//
//        String msg = extraData.getMessage(loggedUser);
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(OneSignalNotificationService.this);
//        builder.setContentTitle(getString(R.string.app_name)).setSmallIcon(R.drawable.flash_screen_logo);
//        PendingIntent pi =
//                PendingIntent.getActivity(OneSignalNotificationService.this,1,
//                        KikiPupActivity.newIntent(OneSignalNotificationService.this,loggedUser,false),
//                        PendingIntent.FLAG_UPDATE_CURRENT);
//        builder.setContentIntent(pi);
//        builder.setContentText(msg);
//        builder.setStyle(new NotificationCompat.BigTextStyle()
//                .bigText(msg));
//        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
//        notificationManager.notify(2,builder.setAutoCancel(true).build());
//        Log.d(TAG, "onDataChange: "+extraData.toMap().toString());

        return true;
    }
}
