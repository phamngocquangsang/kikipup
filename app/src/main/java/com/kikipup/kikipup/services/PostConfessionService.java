package com.kikipup.kikipup.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kikipup.kikipup.models.Post;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Quang Quang on 7/25/2016.
 */
public class PostConfessionService extends IntentService {
    private static final String TAG = PostConfessionService.class.getSimpleName();
    public static String ARG_CONFESSION_EXTRA = "arg_confession_extra";

    private Post mPost;

    private AtomicInteger mUpdateImageTaskCount;

    public static Intent newIntent(Context c, Post post){
        Intent i = new Intent(c, PostConfessionService.class);
        Bundle b = new Bundle();
//        b.putParcelable(ARG_CONFESSION_EXTRA,post);
        i.putExtras(b);
        return i;
    }

    public PostConfessionService() {
        super(PostConfessionService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mPost = intent.getExtras().getParcelable(ARG_CONFESSION_EXTRA);

//        int imageCounter = 0;
//        ArrayList<PostElement> listImageElem = new ArrayList<>();
//        for (PostElement p : mPost.getConfession()) {
//            if(p.getType() == PostElement.IMAGE_TYPE){
//                imageCounter ++;
//                listImageElem.add(p);
//            }
//        }
//        mUpdateImageTaskCount = new AtomicInteger(imageCounter);
//
//        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child("images");
//        for (int i = 0; i < listImageElem.size(); i++) {
//            final PostElement imageElement = listImageElem.get(i);
//            Uri file = Uri.fromFile(new File(imageElement.getContent()));
//            Log.i(TAG, "onHandleIntent: image path: "+imageElement.getContent());
//            UploadTask uploadTask = storageRef.putFile(file);
//            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                    imageElement.setContent(taskSnapshot.getDownloadUrl().toString());
//                    Log.i(TAG, "onComplete: downloadUrl: "+imageElement.getContent());
//                    mUpdateImageTaskCount.decrementAndGet();
//                    postConfession();
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    imageElement.setContent(null);
//                    Log.e(TAG, "onFailure: "+e.getMessage());
//                    mUpdateImageTaskCount.decrementAndGet();
//                    postConfession();
//                }
//            });
//        }
//        postConfession();
    }
    private void postConfession(){
//        if(mUpdateImageTaskCount.get()==0){
//            Post.postNewConfession(this, mPost, new DatabaseReference.CompletionListener() {
//                @Override
//                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                    Log.d(TAG, "postNewConfession:onComplete:" + databaseError);
//                    if (databaseError == null) {
//
//                    }
//
//                }
//            });
//        }

    }


}
