package com.kikipup.kikipup.DI;


import com.kikipup.kikipup.activities.PostDetailActivity;
import com.kikipup.kikipup.login.LoginPresenter;
import com.kikipup.kikipup.mainscreen.KikiActivity;
import com.kikipup.kikipup.viewmodel.ComposerViewModel;
import com.kikipup.kikipup.viewmodel.KikiViewModel;
import com.kikipup.kikipup.viewmodel.PostDetailViewModel;
import com.kikipup.kikipup.viewmodel.PostListViewModel;
import com.kikipup.kikipup.viewmodel.TrendingViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DataModule.class})
public interface AppComponent {

    void inject(KikiActivity activity);

    void inject(LoginPresenter loginPresenter);

    void inject(KikiViewModel viewModel);

    void inject(PostListViewModel viewModel);

    void inject(TrendingViewModel viewModel);

    void inject(ComposerViewModel viewModel);

    void inject(PostDetailViewModel viewModel);
}
