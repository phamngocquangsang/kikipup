package com.kikipup.kikipup.DI;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.kikipup.kikipup.Constants;
import com.kikipup.kikipup.data.DataRepository;
import com.kikipup.kikipup.data.DataSource;
import com.kikipup.kikipup.data.FakeDataSource;
import com.kikipup.kikipup.data.FirebaseDataSource;
import com.kikipup.kikipup.data.LocalSetting;
import com.kikipup.kikipup.data.SharedPreferencesSetting;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Singleton
    @Provides
    DataSource provideDataSource(FirebaseDataSource firebaseDataSource, Application application){
        return new DataRepository(firebaseDataSource);
//        return new FakeDataSource(application);
    }

    @Singleton
    @Provides FirebaseDataSource provideFirebaseDataSource(Application application){
        return new FirebaseDataSource(application);
    }

    @Singleton
    @Provides
    SharedPreferences provideSharedPreference(Application context){
        return context.getSharedPreferences(Constants.KIKI_PUP_PREFERENCE, Context.MODE_PRIVATE);
    }

    @Singleton
    @Provides
    LocalSetting provideLocalSetting(Application c){
        return SharedPreferencesSetting.getInstance(c);
    }
}
