package com.kikipup.kikipup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.format.DateUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kikipup.kikipup.login.LoginActivity;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Quang Quang on 7/17/2016.
 */
public class Utilities {
    private static final String TAG = Utilities.class.getSimpleName();

    public static String FONT_LIGHT = "fonts/OpenSans-Light.ttf";
    public static String FONT_REGULAR = "fonts/OpenSans-Regular.ttf";
    public static String FONT_BOLD = "fonts/RobotoSlab-Bold.ttf";
    public static String FONT_SEMI_BOLD = "fonts/OpenSans-Semibold.ttf";

    public static String encodeEmail(String email){
        return email.replace(".",",");
    }
    public static String decodeEmail(String encodedEmail){
        return encodedEmail.replace(',','.');
    }

    public static String encode(String text)
    {
        byte[] data = new byte[0];
        try {
            data = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static String decode(String text)
    {
        byte[] data = Base64.decode(text, Base64.DEFAULT);
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String bitmapToString(Bitmap b)
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        String s = Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT);
        return s;
    }

    public static Bitmap stringToBitMap(String encodedString){
        try {
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    public static Typeface getTypeface(Context context,String fontName)
    {
        return Typeface.createFromAsset(context.getAssets(), fontName);
    }




    public static String relativeTimeFormat(long timeStamp){
//        final String pattern = "EEE MMM dd HH:mm:ss ZZZZZ yyyy";
//        //2016-05-03 07:58:41
//        final String pattern1 = "yyyy-MM-dd hh:mm:ss";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern1, Locale.ENGLISH);
//        simpleDateFormat.setLenient(true);

        String relativeTime = "";

        relativeTime = DateUtils.getRelativeTimeSpanString(timeStamp, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        return relativeTime;

    }

    public static int getScreenWidth(Activity activity)
    {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    public static int getScreenHeight(Activity activity)
    {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }
    public static void updateLoggedUser (Context c, User user){
        SharedPreferences setting = c.getSharedPreferences(Constants.KIKI_PUP_PREFERENCE,Context.MODE_PRIVATE);
        String userJson = new Gson().toJson(user);
        setting.edit().putString(Constants.LOGGED_USER_JSON ,userJson).apply();
    }

    public static User getLoggedUser (Context c){
        SharedPreferences setting = c.getSharedPreferences(Constants.KIKI_PUP_PREFERENCE,Context.MODE_PRIVATE);
        User user = new Gson().fromJson(setting.getString(Constants.LOGGED_USER_JSON,""),User.class);
        if(user == null){
            return User.getTemporaryUser();
        }
        return user;
    }

//    public static void updateUserList (Context c, UserList userList){
//        SharedPreferences setting = c.getSharedPreferences(Constants.KIKI_PUP_PREFERENCE,Context.MODE_PRIVATE);
//        String userJson = new Gson().toJson(userList);
//        setting.edit().putString(Constants.USERLIST_PREFERENCE ,userJson).apply();
//    }
//
//    public static UserList getUserList (Context c){
//        SharedPreferences setting = c.getSharedPreferences(Constants.KIKI_PUP_PREFERENCE,Context.MODE_PRIVATE);
//        UserList userList = new Gson().fromJson(setting.getString(Constants.USERLIST_PREFERENCE,""),UserList.class);
//        return userList;
//    }

    public static void requireLogin(Context context){
        Toast.makeText(context, "Vui lòng đăng nhập.", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(context, LoginActivity.class);
        context.startActivity(i);
    }

    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

//    public static void logUserUseApp(Context c, User u){
//        Log.i(TAG, "logUserUseApp: ");
//        DatabaseReference ref = FirebaseDataSource.getInstance().getReference().child(Constants.FIREBASE_LOCATION_LOGS);
//        HashMap<String ,Object> timeStamp = new HashMap<>();
//        timeStamp.put(Constants.FIREBASE_PROPERTY_COMMENT_TIMESTAMP, ServerValue.TIMESTAMP);
//        HashMap<String, Object> data = new HashMap<>();
//        data.put("time",timeStamp);
//        data.put("user",Utilities.encodeEmail(u.getEmail()));
//        data.put("state","start");
//
//        String key = ref.child(Constants.FIREBASE_LOCATION_LOG_APP_OPEN).child(Utilities.encode(u.getEmail())).push().getKey();
//        ref.child(Constants.FIREBASE_LOCATION_LOG_APP_OPEN).child(Utilities.encode(u.getEmail())).child(key).updateChildren(data, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                if(databaseError!=null)
//                    Log.i(TAG, "onComplete: logUserUseAPpp+ "+databaseReference.toString());
//            }
//        });
//
//    }

    public static void logUserUseApp(User u) {
        Log.i(TAG, "logUserUseApp: ");
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_LOGS);
        HashMap<String ,Object> timeStamp = new HashMap<>();
        timeStamp.put(Constants.FIREBASE_PROPERTY_COMMENT_TIMESTAMP, ServerValue.TIMESTAMP);
        HashMap<String, Object> data = new HashMap<>();
        data.put("time",timeStamp);
        data.put("user",Utilities.encodeEmail(u.getEmail()));
        data.put("state","startUsingApp");

        String key = ref.child(Constants.FIREBASE_LOCATION_LOG_APP_OPEN).child(Utilities.encodeEmail(u.getEmail())).push().getKey();
        ref.child(Constants.FIREBASE_LOCATION_LOG_APP_OPEN).child(Utilities.encodeEmail(u.getEmail())).child(key).setValue(data);
    }

    public static void logUserLeaveApp(User u) {
        Log.i(TAG, "logUserLeaveApp: ");
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_LOGS);
        HashMap<String ,Object> timeStamp = new HashMap<>();
        timeStamp.put(Constants.FIREBASE_PROPERTY_COMMENT_TIMESTAMP, ServerValue.TIMESTAMP);
        HashMap<String, Object> data = new HashMap<>();
        data.put("time",timeStamp);
        data.put("user",Utilities.encodeEmail(u.getEmail()));
        data.put("state","leaving");

        String key = ref.child(Constants.FIREBASE_LOCATION_LOG_APP_LEAVE).child(Utilities.encodeEmail(u.getEmail())).push().getKey();
        ref.child(Constants.FIREBASE_LOCATION_LOG_APP_LEAVE).child(Utilities.encodeEmail(u.getEmail())).child(key).setValue(data);
    }

//    public static void logUserLeaveApp(Context c, User u){
//        Log.i(TAG, "logUserLeaveApp: ");
//        DatabaseReference ref = FirebaseDataSource.getInstance().getReference().child(Constants.FIREBASE_LOCATION_LOGS);
//        HashMap<String ,Object> timeStamp = new HashMap<>();
//        timeStamp.put(Constants.FIREBASE_PROPERTY_COMMENT_TIMESTAMP, ServerValue.TIMESTAMP);
//        HashMap<String, Object> data = new HashMap<>();
//        data.put("time",timeStamp);
//        data.put("user",Utilities.encodeEmail(u.getEmail()));
//        data.put("state","leave");
//
//        String key = ref.child(Constants.FIREBASE_LOCATION_LOG_APP_LEAVE).child(Utilities.encode(u.getEmail())).push().getKey();
//        ref.child(Constants.FIREBASE_LOCATION_LOG_APP_LEAVE).child(Utilities.encode(u.getEmail())).child(key).updateChildren(data, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                Log.i(TAG, "onComplete: logUserLeaveApp");
//            }
//        });
//
//    }

    public static void logUserCancelLogin(Context c, User u){
        Log.i(TAG, "logUserCancelLogin: ");
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_LOGS);
        HashMap<String ,Object> timeStamp = new HashMap<>();
        timeStamp.put(Constants.FIREBASE_PROPERTY_COMMENT_TIMESTAMP, ServerValue.TIMESTAMP);
        HashMap<String, Object> data = new HashMap<>();
        data.put("time",timeStamp);
        data.put("user",u.getEmail());
        data.put("model", Build.MODEL);
        data.put("manufacturer",Build.MANUFACTURER);

        String key = ref.child(Constants.FIREBASE_LOCATION_LOG_APP_CANCEL_LOGIN).child("anonymous@icychat,com").push().getKey();
        ref.child(Constants.FIREBASE_LOCATION_LOG_APP_CANCEL_LOGIN).child("anonymous@icychat,com").child(key).updateChildren(data);
    }

    public static void logUserReadConfession(User u, Post p) {
        Log.i(TAG, "logUserReadPost: ");
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_LOCATION_LOGS);
        HashMap<String ,Object> timeStamp = new HashMap<>();
        timeStamp.put(Constants.FIREBASE_PROPERTY_COMMENT_TIMESTAMP, ServerValue.TIMESTAMP);
        HashMap<String, Object> data = new HashMap<>();
        data.put("time",timeStamp);
        data.put("post",Post.toMap(p));

        String key = ref.child(Constants.FIREBASE_LOCATION_LOG_USER_READ).child(Utilities.encodeEmail(u.getEmail())).push().getKey();
        ref.child(Constants.FIREBASE_LOCATION_LOG_USER_READ).child(Utilities.encodeEmail(u.getEmail())).child(key).setValue(data);
    }
    public static void unregisterPushNotification(User user){
        user.setOSUserId(null);
        user.setRegistrationId(null);
//        User.updateUser(user, null);
    }
    public static void registerPushNotification(User mUser,String userId, String registrationId) {
        mUser.setRegistrationId(registrationId);
        mUser.setOSUserId(userId);
//        User.updateUser(mUser, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                Log.i(TAG, "onComplete update User OSUID");
//            }
//        });
        Log.d(TAG, "User:" + userId);
        if (registrationId != null)
            Log.d(TAG, "registrationId:" + registrationId);
    }

    public static boolean detectValidURL(String potentialUrl)
    {
        return Patterns.WEB_URL.matcher(potentialUrl).matches();

    }

    public static boolean logginRequire(Context c, User loggedUser) {
        if(loggedUser.getEmail().equals(Constants.ANONYMOUS_EMAIL)){
            Toast.makeText(c, "Vui lòng đăng nhập...", Toast.LENGTH_SHORT).show();
            c.startActivity(new Intent(c,LoginActivity.class));
            return true;
        }
        return false;
    }

//    public static void savingNewsfeed(Context c, List<Post> posts){
//        SharedPreferences pref = getSharedPreference(c);
//        String listJson = new Gson().toJson(posts);
//        pref.edit().putString("saving_newfeed",listJson).apply();
//    }
//
//    public static  List<Post>  loadingNewsfeed(Context c){
//        SharedPreferences pref = getSharedPreference(c);
//        Gson gson = new Gson();
//        String listJson =
//                pref.getString("saving_newfeed", null);
//        Log.i(TAG, "loadingNewsfeed: "+listJson);
//        List<Post> posts = new ArrayList<>();
//        if(listJson!=null){
//            Type listOfPostObject = new TypeToken<List<Post>>(){}.getType();
//            posts = gson.fromJson(listJson,listOfPostObject);
//        }
//        return posts;
//    }

//    public static long formatInvalidLong(String longString){
//        String[] spliter = longString.split("E");
//        if(spliter.length==2){
//            double d = Double.parseDouble(spliter[0]);
//            int power = Integer.parseInt(spliter[1]);
//            return Double.tolong(d*(Math.pow(10,power)));
//
//        }
//    }


    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }


}
