/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kikipup.kikipup.binding;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.Utilities;
import com.kikipup.kikipup.models.Media;
import com.kikipup.kikipup.models.Post;

import java.util.HashMap;
import java.util.Map;

/**
 * Data Binding adapters specific to the app.
 */
public class BindingAdapters {
    private static final String TAG = BindingAdapter.class.getSimpleName();

    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("imageUri")
    public static void loadImage(ImageView view, String imageUri){
        Log.i(TAG, "loadImage: uri = "+imageUri);
        Glide.with(view.getContext()).load(imageUri).into(view);
    }

    @BindingAdapter("bindMedia")
    public static void loadImageMedia(ImageView view, @Nullable Media media){
        if(media == null){
            return;
        }
        int screenWidth;
        ViewGroup.LayoutParams previousLayoutParams = view.getLayoutParams();
        screenWidth = Utilities.getScreenWidth(scanForActivity(view.getContext()));

        if(media.getWidth() != 0 && media.getHeight() != 0){
            previousLayoutParams.height = screenWidth * media.getHeight() / media.getWidth();
        }else{
            previousLayoutParams.height = ActionBar.LayoutParams.WRAP_CONTENT;
        }
        view.setLayoutParams(previousLayoutParams);
        Glide.with(view.getContext()).load(media.getMediaUrl()).into(view);
        Log.i(TAG, "loadImageMedia: uri:" +media.getMediaUrl());

    }

    private static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity)cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper)cont).getBaseContext());

        return null;
    }

    @BindingAdapter("bindComment")
    public static void bindComment(TextView textView, int commentCount){
        String finalComment = commentCount + " " +textView.getResources().getString(R.string.comments);
        textView.setText(finalComment);
    }

    @BindingAdapter({"activePosts" , "postPid", "activeDrawable", "inactiveDrawable"})
    public static void bindUserLike(ImageView view, Map<String, Boolean> activePosts,
                                    String postPid, Drawable active, Drawable inactive){
        view.setImageDrawable(activePosts == null || !activePosts.containsKey(postPid) ? inactive : active);
    }

//    @BindingAdapter("isReactionActive")
//    public static void bindReactionActive(ImageView view, Map<String, Boolean>likePosts, Post post){
//        if(likePosts.containsKey(post.getPostPid())){
//            Glide.with(view.getContext()).load(R.drawable.heart_active);
//        }else{
//            Glide.with(view.getContext()).load(R.drawable.heart_unactive);
//        }
//    }

}
