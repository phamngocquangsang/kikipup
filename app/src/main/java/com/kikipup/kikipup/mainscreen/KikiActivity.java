package com.kikipup.kikipup.mainscreen;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.kikipup.kikipup.Constants;
import com.kikipup.kikipup.R;
import com.kikipup.kikipup.activities.ComposerActivity;
import com.kikipup.kikipup.activities.TrendingActivity;
import com.kikipup.kikipup.databinding.ActivityKikiBinding;
import com.kikipup.kikipup.fragments.PostFragment;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.viewmodel.KikiViewModel;

import java.util.List;
import java.util.Map;

public class KikiActivity extends AppCompatActivity implements LifecycleRegistryOwner {

    private static final String TAG = KikiActivity.class.getSimpleName();
    
    private LifecycleRegistry mLifecycle = new LifecycleRegistry(this);
    
    private ActivityKikiBinding mBinding;
    
    private KikiCallback mKikiCallback;
    private String mUserId;

    @Override
    public LifecycleRegistry getLifecycle() {
        return mLifecycle;
    }

    private KikiViewModel mKikiViewModel;

    private static final String ARG_USER_ID = "arg-user-id";
    private static final String ARG_ENTER_NEWSFEED = "arg-enter-newsfeed";
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_stories:
                    return true;
                case R.id.navigation_create_post:
                    startActivity(ComposerActivity.newIntent(KikiActivity.this, mUserId, null));
                    return true;
                case R.id.navigation_notifications:
                    return true;
                case R.id.navigation_profile:
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_USER_ID, mUserId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        mUserId = savedInstanceState == null
                ? getIntent().getStringExtra(ARG_USER_ID)
                : savedInstanceState.getString(ARG_USER_ID);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_kiki);



        mBinding.setCallback(new KikiCallback() {
            @Override
            public void onTrending() {
                startActivity(TrendingActivity.getIntent(KikiActivity.this, mUserId));
                Log.i(TAG, "onTrending: ");
            }

            @Override
            public void onCat() {
                Log.i(TAG, "onCat: ");
            }

            @Override
            public void onDog() {
                Log.i(TAG, "onDog: ");
            }

            @Override
            public void onOther() {
                Log.i(TAG, "onOther: ");
            }
        });

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        final PostFragment mPostListFragment =
                PostFragment.newInstance(getIntent().getStringExtra(ARG_USER_ID));

        getSupportFragmentManager().beginTransaction().add(R.id.container, mPostListFragment).commit();

        mKikiViewModel = ViewModelProviders.of(this).get(KikiViewModel.class);
        mKikiViewModel.initializeUserId(mUserId);
        mKikiViewModel.loadUserNewsFeed().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> posts) {
                Log.i(TAG, "onChanged: size: "+posts.size());
                mPostListFragment.setData(posts);
            }
        });

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_POSTS);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<Map<String, Post>> type = new GenericTypeIndicator<Map<String, Post>>(){

                };
                Map<String, Post> results = dataSnapshot.getValue(type);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static Intent newIntent(Context context, String userId, boolean enterNewsFeed) {
        Intent i = new Intent(context, KikiActivity.class);
        i.putExtra(ARG_USER_ID, userId);
        i.putExtra(ARG_ENTER_NEWSFEED, enterNewsFeed);
        return i;
    }
    
    public interface KikiCallback{
        
        void onTrending();
        
        void onCat();
        
        void onDog();
        
        void onOther();
    }
}
