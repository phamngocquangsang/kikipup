package com.kikipup.kikipup.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class DataRepository implements DataSource{

    private static final String TAG = DataRepository.class.getSimpleName();
    private final DataSource mFirebaseDb;

    @Override
    public void createNewUser(@NonNull User newUser, OnResponse<User> response) {
        mFirebaseDb.createNewUser(newUser, response);
    }

    @Override
    public void getUser(@NonNull String userId, OnResponse<User> response) {
        mFirebaseDb.getUser(userId, response);
    }

    public DataRepository(@NonNull FirebaseDataSource firebaseDb) {
        checkNotNull(firebaseDb);
        mFirebaseDb = checkNotNull(firebaseDb, "Firebase DB cannot be null");
    }

    @Override
    public LiveData<List<Post>> loadUserNewsfeed(String userId, int count) {
        return mFirebaseDb.loadUserNewsfeed(userId, count);
    }

    @Override
    public LiveData<List<Post>> loadTrending(String userId, int count) {
        return mFirebaseDb.loadTrending(userId, count);
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLikePost(String userId) {
        return mFirebaseDb.loadUserLikePost(userId);
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserDislikePost(String userId) {
        return mFirebaseDb.loadUserDislikePost(userId);
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserSadPost(String userId) {
        return mFirebaseDb.loadUserSadPost(userId);
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLovePost(String userId) {
        return mFirebaseDb.loadUserLovePost(userId);
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLaughPost(String userId) {
        return mFirebaseDb.loadUserLaughPost(userId);
    }

    @Override
    public void likePost(String userId, String postPid) {
        mFirebaseDb.likePost(userId, postPid);
    }

    @Override
    public void dislikePost(String userId, String postPid) {
        mFirebaseDb.dislikePost(userId, postPid);
    }

    @Override
    public void lovePost(String userId, String postPid) {
        mFirebaseDb.lovePost(userId, postPid);
    }

    @Override
    public void sadPost(String userId, String postPid) {
        mFirebaseDb.sadPost(userId,postPid);
    }

    @Override
    public void laughPost(String userId, String postPid) {
        mFirebaseDb.laughPost(userId, postPid);
    }

    @Override
    public void createPost(Post post) {
        Log.i(TAG, "createPost: ");
        mFirebaseDb.createPost(post);
    }

    @Override
    public void updatePost(Post post) {
        mFirebaseDb.updatePost(post);
    }

    @Override
    public LiveData<Post> loadPost(String postId) {
        return mFirebaseDb.loadPost(postId);
    }

    @Override
    public void deletePost(@NonNull String postId) {
        checkNotNull(postId);
        mFirebaseDb.deletePost(postId);
    }

    @Override
    public String getLoggedUserId() {
        return mFirebaseDb.getLoggedUserId();
    }

    @Override
    public void saveDraftPost(Post post) {
        mFirebaseDb.saveDraftPost(post);
    }

    @Override
    public void loadDraftPost(OnResponse<Post> response) {
        mFirebaseDb.loadDraftPost(response);
    }

    @Override
    public void clearDraftPost() {
        mFirebaseDb.clearDraftPost();
    }
}
