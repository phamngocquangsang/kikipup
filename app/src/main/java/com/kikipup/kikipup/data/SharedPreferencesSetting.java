package com.kikipup.kikipup.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.kikipup.kikipup.Constants;
import com.kikipup.kikipup.Utilities;
import com.kikipup.kikipup.models.User;

import static com.google.common.base.Preconditions.checkNotNull;

public class SharedPreferencesSetting implements LocalSetting {

    private Context mContext;

    private static LocalSetting INSTANCE = null;

    public SharedPreferencesSetting(Context c) {
        mContext = c;
    }

    public static LocalSetting getInstance(@NonNull Context c) {
        checkNotNull(c);
        if (INSTANCE == null) {
            INSTANCE = new SharedPreferencesSetting(c);
        }
        return INSTANCE;
    }

    @Override
    public void saveLoggedUser(User loggedUser) {
        getSharedPreference(mContext).edit().putString(Constants.LOGGED_USER_JSON,new Gson().toJson(loggedUser)).apply();
    }

    @Override
    public void setUserLogged(boolean isLogged) {
        getSharedPreference(mContext).edit().putBoolean(Constants.IS_LOGGED,isLogged).apply();
    }

    public static SharedPreferences getSharedPreference(Context context) {
        return context.getSharedPreferences(Constants.KIKI_PUP_PREFERENCE, Context.MODE_PRIVATE);
    }
}
