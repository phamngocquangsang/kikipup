package com.kikipup.kikipup.data;

import com.kikipup.kikipup.models.User;

public interface LocalSetting {

    void saveLoggedUser(User loggedUser);

    void setUserLogged(boolean isLogged);

}
