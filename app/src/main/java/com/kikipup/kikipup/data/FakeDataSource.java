package com.kikipup.kikipup.data;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.kikipup.kikipup.KikiPupApp;
import com.kikipup.kikipup.models.Media;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FakeDataSource implements DataSource {

    private static final String TAG = FakeDataSource.class.getSimpleName();

    private final MutableLiveData<List<Post>> fakePosts = new MutableLiveData<>();

    final private MutableLiveData<Map<String, Boolean>> userDislikePosts = new MutableLiveData<>();

    final private MutableLiveData<Map<String, Boolean>> userLovePosts = new MutableLiveData<>();

    final private MutableLiveData<Map<String, Boolean>> userSadPosts = new MutableLiveData<>();

    final private MutableLiveData<Map<String, Boolean>> userLaughPosts = new MutableLiveData<>();

    final private MutableLiveData<Map<String, Boolean>> userlikePosts = new MutableLiveData<>();

    public static List<Post> sFakesPost = new ArrayList<>();
    static{
        for (int i = 0; i < 30; i++) {
            sFakesPost.add(Post.getFakePost());
        }
    }

    private final Application mApplication;

    public FakeDataSource(Application application) {
        mApplication = application;
    }

    @Override
    public void createNewUser(@NonNull User newUser, OnResponse<User> response) {
        response.onSuccess(newUser);
    }

    @Override
    public void getUser(@NonNull String userId, OnResponse<User> response) {

    }

    @Override
    public LiveData<List<Post>> loadUserNewsfeed(String userId, int count) {
        fakePosts.setValue(sFakesPost);
        return fakePosts;
    }

    @Override
    public LiveData<List<Post>> loadTrending(String userId, int count) {
        MutableLiveData<List<Post>> postList = new MutableLiveData<>();
        postList.setValue(sFakesPost);
        return postList;
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLikePost(String UserId) {
        Map<String, Boolean> likePosts = new HashMap<>();
        for (int i = 0; i < 5; i++) {
            likePosts.put(sFakesPost.get(i).getPostPid(), true);
        }
        userlikePosts.setValue(likePosts);
        return userlikePosts;
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserDislikePost(String userId) {
        Map<String, Boolean> dislikePosts = new HashMap<>();
        for (int i = 5; i < 10; i++) {
            dislikePosts.put(sFakesPost.get(i).getPostPid(), true);
        }
        userDislikePosts.setValue(dislikePosts);
        return userDislikePosts;
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserSadPost(String userId) {
        Map<String, Boolean> sadLists = new HashMap<>();
        for (int i = 10; i < 15; i++) {
            sadLists.put(sFakesPost.get(i).getPostPid(), true);
        }
        userSadPosts.setValue(sadLists);
        return userSadPosts;
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLovePost(String userId) {
        Map<String, Boolean> lovePosts = new HashMap<>();
        for (int i = 15; i < 20; i++) {
            lovePosts.put(sFakesPost.get(i).getPostPid(), true);
        }
        userLovePosts.setValue(lovePosts);
        return userLovePosts;
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLaughPost(String userId) {
        Map<String, Boolean> laughPosts = new HashMap<>();
        for (int i = 20; i < 30; i++) {
            laughPosts.put(sFakesPost.get(i).getPostPid(), true);
        }
        userLaughPosts.setValue(laughPosts);
        return userLaughPosts;
    }

    @Override
    public void likePost(String userId, String postPid) {
        Log.i(TAG, "likePost: ");
        Map<String, Boolean> values = userlikePosts.getValue();
        if(values.containsKey(postPid)){
            values.remove(postPid);
        }else{
            values.put(postPid, true);
        }
        userlikePosts.setValue(values);
    }

    @Override
    public void dislikePost(String userId, String postPid) {
        Log.i(TAG, "dislikePost: ");
        Map<String, Boolean> values = userDislikePosts.getValue();
        if(values.containsKey(postPid)){
            values.remove(postPid);
        }else{
            values.put(postPid, true);
        }
        userDislikePosts.setValue(values);
    }

    @Override
    public void lovePost(String userId, String postPid) {
        Log.i(TAG, "lovePost: ");
        Map<String, Boolean> values = userLovePosts.getValue();
        if(values.containsKey(postPid)){
            values.remove(postPid);
        }else{
            values.put(postPid, true);
        }
        userLovePosts.setValue(values);
    }

    @Override
    public void sadPost(String userId, String postPid) {
        Log.i(TAG, "sadPost: ");
        Map<String, Boolean> values = userSadPosts.getValue();
        if(values.containsKey(postPid)){
            values.remove(postPid);
        }else{
            values.put(postPid, true);
        }
        userSadPosts.setValue(values);
    }

    @Override
    public void laughPost(String userId, String postPid) {
        Map<String, Boolean> values = userLaughPosts.getValue();
        if(values.containsKey(postPid)){
            values.remove(postPid);
        }else{
            values.put(postPid, true);
        }
        userLaughPosts.setValue(values);
    }

    @Override
    public void createPost(final Post post) {
        Log.i(TAG, "createPost: ");
        if(post.getMedia() != null){
            final Media processingMedia = new Media();
            Glide.with(mApplication).load(post.getMedia().getMediaUrl()).asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    processingMedia.setHeight(resource.getHeight());
                    processingMedia.setWidth(resource.getWidth());
                    Log.i(TAG, "onResourceReady: Width x heigh = "+resource.getWidth() + "x"+resource.getHeight());
                }
            });
        }
        sFakesPost.add(post);
        fakePosts.setValue(sFakesPost);
    }

    @Override
    public void updatePost(Post post) {
        for (int i = 0; i < sFakesPost.size(); i++) {
            Post p = sFakesPost.get(i);
            if (p.getPostPid().equals(post.getPostPid())) {
                sFakesPost.set(i, post);
                fakePosts.setValue(sFakesPost);
                break;
            }
        }
    }

    @Override
    public LiveData<Post> loadPost(String postId) {
        return null;
    }


    @Override
    public String getLoggedUserId() {
        return null;
    }

    @Override
    public void deletePost(@NonNull String postId) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void saveDraftPost(Post post) {

    }

    @Override
    public void loadDraftPost(OnResponse<Post> response) {

    }

    @Override
    public void clearDraftPost() {

    }
}
