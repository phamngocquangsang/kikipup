package com.kikipup.kikipup.data;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.URLUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kikipup.kikipup.Constants;
import com.kikipup.kikipup.livecycleaware.FirebaseChildPostListener;
import com.kikipup.kikipup.livecycleaware.FirebaseLiveData;
import com.kikipup.kikipup.models.Media;
import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class FirebaseDataSource implements DataSource {

    private static final String TAG = FirebaseDataSource.class.getSimpleName();

    private final Application mApplication;

    private FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();

    public FirebaseDataSource(Application application) {
        mApplication = application;
    }


    @Override
    public void createNewUser(@NonNull final User user, final OnResponse<User> response) {
        DatabaseReference fb = mFirebaseDatabase.getReference();
        fb.child("users").child(user.getUserId()).setValue(user);
    }

    @Override
    public void getUser(@NonNull String userId, final OnResponse<User> onResponse) {
        FirebaseDatabase.getInstance().getReference()
                .child("users").child(userId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        onResponse.onSuccess(dataSnapshot.getValue(User.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        onResponse.onFailure(new DatabaseException(databaseError.getMessage(), databaseError.toException()));
                    }
                });
    }

    @Override
    public LiveData<List<Post>> loadUserNewsfeed(String userId, int count) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_POSTS);
//        FirebaseLiveData<Map<String, Post>> data =
//                new FirebaseLiveData<>(ref, new GenericTypeIndicator<Map<String, Post>>() {});
//
//        return Transformations.map(data, new Function<Map<String, Post>, List<Post>>() {
//            @Override
//            public List<Post> apply(Map<String, Post> stringPostMap) {
//                List<Post> list = new ArrayList<>();
//                if(stringPostMap != null){
//                    for (Map.Entry<String, Post> set: stringPostMap.entrySet()) {
//                        list.add(set.getValue());
//                    }
//                }
//                return list;
//            }
//        });
        final FirebaseLiveData<List<Post>> data = new FirebaseLiveData<>(ref, null);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Post> posts = new ArrayList<>();
                for (DataSnapshot postSnapshot :
                        dataSnapshot.getChildren()) {
                    Post post = postSnapshot.getValue(Post.class);
                    posts.add(post);
                }
                data.setValue(posts);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        data.setListener(valueEventListener);
        return data;
    }

    @Override
    public LiveData<List<Post>> loadTrending(String userId, int count) {
        return null;
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLikePost(String userId) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.LOCATION_USER_LIKE).child(userId);
        return new FirebaseLiveData<>(ref, new GenericTypeIndicator<Map<String, Boolean>>() {});
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserDislikePost(String userId) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.LOCATION_USER_DISLIKE).child(userId);
        return new FirebaseLiveData<>(ref, new GenericTypeIndicator<Map<String, Boolean>>() {});
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserSadPost(String userId) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.LOCATION_USER_SAD).child(userId);
        return new FirebaseLiveData<>(ref, new GenericTypeIndicator<Map<String, Boolean>>() {});
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLovePost(String userId) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.LOCATION_USER_LOVE).child(userId);
        return new FirebaseLiveData<>(ref, new GenericTypeIndicator<Map<String, Boolean>>() {});
    }

    @Override
    public LiveData<Map<String, Boolean>> loadUserLaughPost(String userId) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(Constants.LOCATION_USER_LAUGH).child(userId);
        return new FirebaseLiveData<>(ref, new GenericTypeIndicator<Map<String, Boolean>>() {});
    }

    @Override
    public void likePost(final String userId, final String postPid) {
        Log.i(TAG, "likePost: userId:"+userId);
        //todo cloud function should listen for this change then in/de crease like counter
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_USER_LIKE)
                .child(userId).child(postPid).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, Object> likeUpdate = new HashMap<>();
                        if(dataSnapshot.exists()){
                            //user like this post-> unlike
                            likeUpdate.put(Constants.LOCATION_POST_LIKE+"/"+postPid+"/"+userId, null);
                            likeUpdate.put(Constants.LOCATION_USER_LIKE+"/"+userId+"/"+postPid, null);
                            Log.i(TAG, "unlikePost:");
                        }else{
                            //->like this post
                            likeUpdate.put(Constants.LOCATION_POST_LIKE+"/"+postPid+"/"+userId, true);
                            likeUpdate.put(Constants.LOCATION_USER_LIKE+"/"+userId+"/"+postPid, true);
                            Log.i(TAG, "likePost: ");
                        }
                        FirebaseDatabase.getInstance().getReference().updateChildren(likeUpdate);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    @Override
    public void dislikePost(final String userId, final String postPid) {
        Log.i(TAG, "dislikePost: userId:"+userId);
        //todo cloud function should listen for this change then in/de crease like counter
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_USER_DISLIKE)
                .child(userId).child(postPid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> disLikeUpdate = new HashMap<>();
                if(dataSnapshot.exists()){
                    disLikeUpdate.put(Constants.LOCATION_POST_DISLIKE+"/"+postPid+"/"+userId, null);
                    disLikeUpdate.put(Constants.LOCATION_USER_DISLIKE+"/"+userId+"/"+postPid, null);
                }else{
                    disLikeUpdate.put(Constants.LOCATION_POST_DISLIKE+"/"+postPid+"/"+userId, true);
                    disLikeUpdate.put(Constants.LOCATION_USER_DISLIKE+"/"+userId+"/"+postPid, true);
                }
                FirebaseDatabase.getInstance().getReference().updateChildren(disLikeUpdate);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void lovePost(final String userId, final String postPid) {
        //todo cloud function should listen for this change then in/de crease love counter
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_USER_LOVE)
                .child(userId).child(postPid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> loveUpdate = new HashMap<>();
                if(dataSnapshot.exists()){
                    loveUpdate.put(Constants.LOCATION_POST_LOVE + "/"+postPid+"/"+userId, null);
                    loveUpdate.put(Constants.LOCATION_USER_LOVE+"/"+userId+"/"+postPid, null);
                }else{
                    //->like this post
                    loveUpdate.put(Constants.LOCATION_POST_LOVE+"/"+postPid+"/"+userId, true);
                    loveUpdate.put(Constants.LOCATION_USER_LOVE+"/"+userId+"/"+postPid, true);
                }
                FirebaseDatabase.getInstance().getReference().updateChildren(loveUpdate);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void sadPost(final String userId, final String postPid) {
        //todo cloud function should listen for this change then in/de crease sad counter
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_USER_SAD)
                .child(userId).child(postPid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> sadUpdate = new HashMap<>();
                if(dataSnapshot.exists()){
                    sadUpdate.put(Constants.LOCATION_POST_SAD + "/"+postPid+"/"+userId, null);
                    sadUpdate.put(Constants.LOCATION_USER_SAD+"/"+userId+"/"+postPid, null);
                }else{
                    sadUpdate.put(Constants.LOCATION_POST_SAD+"/"+postPid+"/"+userId, true);
                    sadUpdate.put(Constants.LOCATION_USER_SAD+"/"+userId+"/"+postPid, true);
                }
                FirebaseDatabase.getInstance().getReference().updateChildren(sadUpdate);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void laughPost(final String userId, final String postPid) {
        //todo cloud function should listen for this change then in/de crease laugh counter
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_USER_LAUGH)
                .child(userId).child(postPid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> laughUpdate = new HashMap<>();
                if(dataSnapshot.exists()){
                    laughUpdate.put(Constants.LOCATION_POST_LAUGH + "/"+postPid+"/"+userId, null);
                    laughUpdate.put(Constants.LOCATION_USER_LAUGH+"/"+userId+"/"+postPid, null);
                }else{
                    laughUpdate.put(Constants.LOCATION_POST_LAUGH+"/"+postPid+"/"+userId, true);
                    laughUpdate.put(Constants.LOCATION_USER_LAUGH+"/"+userId+"/"+postPid, true);
                }
                FirebaseDatabase.getInstance().getReference().updateChildren(laughUpdate);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void createPost(final Post post) {
        checkNotNull(post.getUser(), "user can not be null");
        handleMediaBeforeUpload(post, new OnResponse<Post>() {
            @Override
            public void onSuccess(Post type) {
                Map<String, Object> serverTime = new HashMap<>();
                serverTime.put(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP, ServerValue.TIMESTAMP);
                post.setTimeCreated(serverTime);
                post.setLastTimeEdited(serverTime);

                Map<String, Object> createPost = new HashMap<>();
                String key = FirebaseDatabase.getInstance().getReference().child(Constants.LOCATION_POSTS).push().getKey();
                post.setPostPid(key);

                createPost.put(Constants.LOCATION_POSTS + "/" + key, Post.toMap(post));
                createPost.put(Constants.LOCATION_USER_POSTS + "/" + post.getUser().getUserId()+ "/"+ key, Post.toMap(post));

                FirebaseDatabase.getInstance().getReference().updateChildren(createPost);
            }

            @Override
            public void onFailure(Exception exception) {

            }
        });


    }


    @Override
    public void updatePost(final Post post) {
        handleMediaBeforeUpload(post, new OnResponse<Post>() {
            @Override
            public void onSuccess(Post type) {
                Log.i(TAG, "updatePostToServer: ");

                Map<String, Object> serverTime = new HashMap<>();
                serverTime.put(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP, ServerValue.TIMESTAMP);
                post.setLastTimeEdited(serverTime);

                Map<String, Object> updatePost = new HashMap<>();
                Map<String, Object> postMap = Post.toMap(post);

                updatePost.put(Constants.LOCATION_POSTS + "/" +post.getPostPid(), postMap);
                updatePost.put(Constants.LOCATION_USER_POSTS + "/" + post.getPostPid(), postMap);
                FirebaseDatabase.getInstance().getReference().updateChildren(updatePost);
            }

            @Override
            public void onFailure(Exception exception) {

            }
        });


    }

    public void handleMediaBeforeUpload(final Post post, final OnResponse<Post> onResponse) {
        checkNotNull(post.getUser(), "user can not be null");
        Map<String, Object> serverTime = new HashMap<>();
        serverTime.put(Constants.FIREBASE_PROPERTY_POST_TIMESTAMP, ServerValue.TIMESTAMP);
        post.setLastTimeEdited(serverTime);
        if(post.getMedia()!=null){
            if(URLUtil.isContentUrl(post.getMedia().getMediaUrl())
                    || URLUtil.isFileUrl(post.getMedia().getMediaUrl())){//this is local file, need to upload image
                Glide.with(mApplication).load(post.getMedia().getMediaUrl()).asBitmap().into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(final Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                        // Get the data from an ImageView as bytes
                        uploadImage(post.getUser().getUserId(), bitmap, new OnSuccessListener<UploadTask.TaskSnapshot>(){
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Media uploadedMedia = new Media();
                                uploadedMedia.setHeight(bitmap.getHeight());
                                uploadedMedia.setWidth(bitmap.getWidth());
                                uploadedMedia.setMediaUrl(taskSnapshot.getDownloadUrl().toString());
                                uploadedMedia.setType(Media.TYPE_IMAGE);
                                post.setMedia(uploadedMedia);
                                onResponse.onSuccess(post);
                            }
                        }, new OnFailureListener(){
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.i(TAG, "onFailure: ");
                                onResponse.onFailure(e);
                            }
                        });
                    }
                });
            }else{
                onResponse.onSuccess(post);
            }
        }else{
            onResponse.onSuccess(post);
        }
    }

    @Override
    public LiveData<Post> loadPost(String postId) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_POSTS)
                .child(postId);
        return new FirebaseLiveData<>(ref, new GenericTypeIndicator<Post>() {});
    }

    public void uploadImage(String userId, Bitmap bitmap, OnSuccessListener<UploadTask.TaskSnapshot> onSuccessListener
            , OnFailureListener onFailureListener){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                .child("user")
                .child(userId)
                .child(System.currentTimeMillis()+".jpg");
        UploadTask uploadTask = storageReference.putBytes(data);
        uploadTask.addOnFailureListener(onFailureListener).addOnSuccessListener(onSuccessListener);
    }

    @Override
    public String getLoggedUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Override
    public void deletePost(@NonNull final String postId) {
        String userId = getLoggedUserId();
        Map<String, Object> deleteMap = new HashMap<>();
        deleteMap.put(Constants.LOCATION_USER_POSTS+ "/" + userId + "/" + postId, null);
        deleteMap.put(Constants.LOCATION_POSTS + "/" + postId , null);
        FirebaseDatabase.getInstance().getReference().updateChildren(deleteMap);
    }

    @Override
    public void saveDraftPost(Post post) {
        handleMediaBeforeUpload(post, new OnResponse<Post>() {
            @Override
            public void onSuccess(Post type) {
                FirebaseDatabase.getInstance().getReference()
                        .child(Constants.LOCATION_DRAFT_POSTS)
                        .child(getLoggedUserId()).setValue(Post.toMap(type));
            }

            @Override
            public void onFailure(Exception exception) {

            }
        });

    }

    @Override
    public void loadDraftPost(final OnResponse<Post> response) {
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_DRAFT_POSTS)
                .child(getLoggedUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Post post = dataSnapshot.getValue(Post.class);
                        if(post == null){
                            response.onSuccess(new Post());
                        }else{
                            response.onSuccess(post);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        response.onFailure(databaseError.toException());
                    }
                });
    }

    @Override
    public void clearDraftPost() {
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.LOCATION_DRAFT_POSTS)
                .child(getLoggedUserId()).setValue(null);
    }
}
