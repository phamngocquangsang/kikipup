/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kikipup.kikipup.data;


import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;


import com.kikipup.kikipup.models.Post;
import com.kikipup.kikipup.models.User;

import java.util.List;
import java.util.Map;

/**
 * Main entry point for accessing tasks data.
 * <p>
 * For simplicity, only getTasks() and getTask() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new task is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */
public interface DataSource {

    public class DatabaseException extends RuntimeException {

        public DatabaseException(String var1) {
            super(var1);
        }

        public DatabaseException(String var1, Throwable var2) {
            super(var1, var2);
        }
    }

    interface GetUserCallback {

        void onUserLoaded(User user);

        void onError(DatabaseException exception);
    }

    interface OnResponse<T>{

        void onSuccess(T type);

        void onFailure(Exception exception);
    }

    String getLoggedUserId();

    void createNewUser(@NonNull User newUser, OnResponse<User> response);

    void getUser(@NonNull String userId, OnResponse<User> response);

    LiveData<List<Post>> loadUserNewsfeed(String userId, int count);

    LiveData<List<Post>> loadTrending(String userId, int count);

    LiveData<Map<String, Boolean>> loadUserLikePost(String userId);

    LiveData<Map<String, Boolean>> loadUserDislikePost(String userId);

    LiveData<Map<String, Boolean>> loadUserSadPost(String userId);

    LiveData<Map<String, Boolean>> loadUserLovePost(String userId);

    LiveData<Map<String, Boolean>> loadUserLaughPost(String userId);

    void likePost(String userId, String postPid);

    void dislikePost(String userId, String postPid);

    void lovePost(String userId, String postPid);

    void sadPost(String userId, String postPid);

    void laughPost(String userId, String postPid);

    void createPost(Post post);

    void updatePost(Post post);

    void deletePost(@NonNull String postId);

    LiveData<Post> loadPost(String postId);

    void saveDraftPost(Post post);

    void loadDraftPost(OnResponse<Post> response);

    void clearDraftPost();

}
