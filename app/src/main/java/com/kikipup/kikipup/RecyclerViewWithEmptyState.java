package com.kikipup.kikipup;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by phatnguyen on 8/2/16.
 */
public class RecyclerViewWithEmptyState extends RecyclerView {
    View emptyView;

    public RecyclerViewWithEmptyState(Context context) {
        super(context);
    }

    public RecyclerViewWithEmptyState(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewWithEmptyState(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private AdapterDataObserver emptyObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            toggleEmptyView();

        }
    };

    @Override
    public void setAdapter(Adapter adapter) {
        super.setAdapter(adapter);

        if(adapter!=null)
        {
            adapter.registerAdapterDataObserver(emptyObserver);
        }

        emptyObserver.onChanged();
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
    }
    public void toggleEmptyView()
    {
        Adapter<?> adapter = getAdapter();
        if(adapter!=null &&emptyView!=null)
        {
            if(adapter.getItemCount()==0)
            {
                emptyView.setVisibility(View.VISIBLE);
                RecyclerViewWithEmptyState.this.setVisibility(View.GONE);
            }else
            {
                emptyView.setVisibility(View.GONE);
                RecyclerViewWithEmptyState.this.setVisibility(View.VISIBLE);
            }
        }

    }
}
